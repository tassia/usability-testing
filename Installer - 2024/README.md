# Usability testing in Debian: Installer - 2024

We use this repo to collaborate on the planning, execution and result
consolidation of usability tests in Debian. This folder contains documents
from the second phase of the project, that took place in 2024.

## The Debian installation process

This round of tests aimed to investigate if there is a correlation between 
a user's perspective on Debian and its installation process and which
installer was being used.

We have observed the process of installing Debian using the standard installer
(netinst) and the Calamares installer (live image). Then we compared the
difficulty level experienced with both from the perspective of new users to
Debian (and to GNU/Linux in most cases).

Instructions to [testers](instructions/instruction-testers.md) and
[observers](instruction-observers.md) provide more details about the setup and
unfolding of tests.  

## Timeline

* Pilot run: May 2024
* First phase: June 1st - 9th, 2024

## Acknowledgement

The computers used for testing installations was funded by the Debian
Project and by the Eastern Bloc organization in Montreal. Thanks to all
donors for [supporting Debian](https://www.debian.org/donations).

## Authors

This study is being conducted by Tassia Camoes Araujo, Anthony Nadeau, 
Giuliana Bouzon and Justin Bax.

## Contributing

Comments, suggestions and merge requests are more than welcome! You can reach us
through email, IRC #usability-testing (OFTC server) or through
[Discord](https://discord.gg/rBKb9JRQGD).

We are also open to organizing usability tests in other areas of Debian or FLOSS
in general.