# Outstanding results

## Hypotheses

We had 4 explanatory variables (which installer is used, technical ability, OS experience, Linux experience) and 3 response variables (time taken, help needed, difficulty rating). The hypotheses we wanted to verify is each explanatory variables were correlated with each response variables.

### Installer used && difficulty rating

n=36, p=0.0048 (Chi square), p=0.0003 (Mantel-Haenszel)

***Netinst was found to be given a higher difficulty rating than Calamares***

### Installer used && time taken (total)

n=31, p=0.0006 (Median test)

***Netinst was found to have higher total time***

### Installer used && time taken (by parts)

In the categories **finding the iso**, **configuring users**, **partitioning** and **installing**:

n>=35, p<=0.0027 (Median test)

***Netinst was found to have lower time taken in these categories***

In all other categories (**basics** and **configuring network**):

n>=35, p>=0.2344 (Median test)

***We cannot accept the alternate hypothesis***

### Installer used && help needed (total)

n=31, p=0.1489 (Fisher's exact test)

***We cannot accept the alternate hypothesis***

### Installer used && help needed (by parts)

In **all** categories:

n>=35, p<=0.0001 (Fisher's exact test)

***Netinst was found to require more help in all categories***

*In the following sections, only conclusive results are stated. All other alternate hypotheses were rejected.*

### Technical ability && time taken (total, netinst)

n=15, p=0.0036 (Mantel-Haenszel chi square)

***Testers with higher technical ability were found to complete the netinst installation faster***

### Linux experience && time taken (total, netinst)

n=15, p=0.0489 (Mantel-Haenszel chi square)

***Testers with higher Linux experience were found to complete the netinst installation faster***

### OS experience && time taken (finding the iso, Calamares)

n=18, p=0.0173 (Mantel-Haenszel chi square)

***Testers with higher OS experience were found to be able to find the Calamares iso faster***

## Medians, extrema

### Difficulty rating

Netinst (mean): 3.28

Calamares (mean): 1.61

The following lines are formatted as such (times are in `MM:SS`):

```txt
category: median (min - max)
```

### Whole test (valid times only)

Whole test time: 83:52 (56:00 - 105:09)

Whole test help: 2 (0 - 9)

### Total by section
<!--  -->
Total netinst: 55:45 (34:04 - 70:36)

Total Calamares: 34:51 (20:31 - 55:49)

### Finding the iso

Netinst iso: 1:06 (0:12 - 3:30)

Calamares iso: 3:37 (0:43 - 19:41)

### Basics: location, keyboard, language

Netinst basics: 0:55 (0:10 - 7:00)

Calamares basics: 0:30 (0:03 - 1:02)

### Network configuration

Netinst network: 2:19 (0:45 - 16:42)

Calamares network: 1:33 (0:10 - 2:59)

### Users configuration

Netinst users: 1:13 (0:09 - 6:50)

Calamares users: 0:31 (0:05 - 1:20)

### Disk partitioning

Netinst partitions: 1:16 (0:37 - 9:00)

Calamares partitions: 0:15 (0:05 - 4:02)

### Install time

Netinst install: 32:40 (19:05 - 55:13)

Calamares install 14:10 (12:46 - 15:15)
