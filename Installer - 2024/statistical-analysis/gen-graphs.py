import matplotlib.pyplot as plt
from sys import argv
import csv
from statistics import mean
from numpy import quantile

from scipy import stats
from scipy.ndimage import median

def print_stats_info(netinst_data, calamares_data, first="Netinst", second="Calamares"):
    stats_1 = quantile(netinst_data, [0,0.25,0.5,0.75,1])
    stats_2 = quantile(calamares_data, [0,0.25,0.5,0.75,1])
    print(f"{first}")
    # print(f"  mean   {mean(netinst_data)}")
    print(f"  median {stats_1[2]}")
    print(f"  max    {stats_1[4]}")
    print(f"  min    {stats_1[0]}")
    # print(f"  Q_1    {stats_1[1]}")
    # print(f"  Q_3    {stats_1[3]}")
    print(f"{second}")
    # print(f"  mean   {mean(calamares_data)}")
    print(f"  median {stats_2[2]}")
    print(f"  max    {stats_2[4]}")
    print(f"  min    {stats_2[0]}")
    # print(f"  Q_1    {stats_2[1]}")
    # print(f"  Q_3    {stats_2[3]}")
    print()

COLUMNS_FOR_NETINST_CUMULATIVE = [
    "A.1", "A.3", "A.4",
    "1B.1", "1B.2", "1B.3", "1B.4", "1B.5", "1B.6",
    "1B.7", "1B.8", "1B.9", "1B.10", "1B.11", "1B.12",
    "1B.13", "1B.14",
]

COLUMNS_FOR_CALAMARES_CUMULATIVE = [
    "A.2", "A.3", "A.4",
    "2B.1", "2B.2", "2B.3", "2B.4", "2B.5", "2B.6",
    "2B.7", "2B.8", "2B.9", "2B.10",
]

COLUMNS_FOR_ALL = [
    "A.1", "A.2", "A.3", "A.4",
    "1B.1", "1B.2", "1B.3", "1B.4", "1B.5", "1B.6",
    "1B.7", "1B.8", "1B.9", "1B.10", "1B.11", "1B.12",
    "1B.13", "1B.14",
    "2B.1", "2B.2", "2B.3", "2B.4", "2B.5", "2B.6",
    "2B.7", "2B.8", "2B.9", "2B.10",
]

def main():
    # This whole setup phase is copy-pasted from stats.py
    if len(argv) != 2:
        print("Incorrect usage")
        print("Correct usage: python stats.py [results_csv]")
        exit(1)

    results_path = argv[1]
    results = []
    row_indices = {}

    with open(results_path, newline="") as results_file:
        parser = csv.reader(results_file, dialect="unix")

        # Parse first line of results (column names)
        column_names = parser.__next__()
        for index, column_name in enumerate(column_names):
            row_indices[column_name] = index

        # Get all the data
        for row in parser:
            # We need these "cumulative" info (for each of time and help):
            # The wildcard (*) is replaced by "time" and "help"
            # key "1B cumulative *"     ==> A.(1+3+4) + 1B.*
            # key "2B cumulative *"     ==> A.(2+3+4) + 2B.*
            # key "1B network *"        ==> 1B.(3+4)
            # key "1B install *"        ==> 1B.(8+10+11)

            total_netinst_time = 0
            total_calamares_time = 0
            total_netinst_help = 0
            total_calamares_help = 0
            total_netinst_is_valid = True
            total_calamares_is_valid = True
            total_with_invalid_time = 0
            total_no_invalid_time = 0
            total_with_invalid_help = 0
            total_no_invalid_help = 0
            total_is_valid = True

            for task in COLUMNS_FOR_NETINST_CUMULATIVE:
                time_column_name = task + " time"
                help_column_name = task + " help"
                if row[row_indices[time_column_name]].isnumeric() and row[row_indices[help_column_name]].isnumeric():
                    total_netinst_time += int(row[row_indices[time_column_name]])
                    total_netinst_help += int(row[row_indices[help_column_name]])
                else:
                    total_netinst_is_valid = False
                    break

            for task in COLUMNS_FOR_CALAMARES_CUMULATIVE:
                time_column_name = task + " time"
                help_column_name = task + " help"
                try:
                    total_calamares_time += int(row[row_indices[time_column_name]])
                    total_calamares_help += int(row[row_indices[help_column_name]])
                except:
                    total_calamares_is_valid = False

            for task in COLUMNS_FOR_ALL:
                time_column_name = task + " time"
                help_column_name = task + " help"
                try:
                    total_with_invalid_time += int(row[row_indices[time_column_name]])
                    total_no_invalid_time += int(row[row_indices[time_column_name]])
                    total_with_invalid_help += int(row[row_indices[help_column_name]])
                    total_no_invalid_help += int(row[row_indices[help_column_name]])
                except:
                    total_is_valid = False

            network_netinst_time = 0
            network_netinst_help = 0
            network_netinst_is_valid = True

            try:
                network_netinst_time += int(row[row_indices["1B.3 time"]])
                network_netinst_time += int(row[row_indices["1B.4 time"]])
                network_netinst_help += int(row[row_indices["1B.3 help"]])
                network_netinst_help += int(row[row_indices["1B.4 help"]])
            except:
                network_netinst_is_valid = False

            install_netinst_time = 0
            install_netinst_help = 0
            install_netinst_is_valid = True
            
            try:
                install_netinst_time += int(row[row_indices["1B.8 time"]])
                install_netinst_time += int(row[row_indices["1B.10 time"]])
                install_netinst_time += int(row[row_indices["1B.11 time"]])
                install_netinst_help += int(row[row_indices["1B.8 help"]])
                install_netinst_help += int(row[row_indices["1B.10 help"]])
                install_netinst_help += int(row[row_indices["1B.11 help"]])
            except:
                install_netinst_is_valid = False

            row_with_additional_info = row
            
            # We convert the numbers back to strings simply because all variables are strings
            row_with_additional_info.append(str(total_netinst_time) if total_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(total_netinst_help) if total_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(total_calamares_time) if total_calamares_is_valid else "N/A")
            row_with_additional_info.append(str(total_calamares_help) if total_calamares_is_valid else "N/A")
            row_with_additional_info.append(str(network_netinst_time) if network_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(network_netinst_help) if network_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(install_netinst_time) if install_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(install_netinst_help) if install_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(total_no_invalid_time) if total_is_valid else "N/A")
            row_with_additional_info.append(str(total_no_invalid_help) if total_is_valid else "N/A")
            row_with_additional_info.append(str(total_with_invalid_time))
            row_with_additional_info.append(str(total_with_invalid_help))

            row_indices["1B cumulative time"] = len(row_with_additional_info) - 12
            row_indices["1B cumulative help"] = len(row_with_additional_info) - 11
            row_indices["2B cumulative time"] = len(row_with_additional_info) - 10
            row_indices["2B cumulative help"] = len(row_with_additional_info) - 9
            row_indices["1B network time"] = len(row_with_additional_info) - 8
            row_indices["1B network help"] = len(row_with_additional_info) - 7
            row_indices["1B install time"] = len(row_with_additional_info) - 6
            row_indices["1B install help"] = len(row_with_additional_info) - 5
            row_indices["Total valid time"] = len(row_with_additional_info) - 4
            row_indices["Total valid help"] = len(row_with_additional_info) - 3
            row_indices["Total time"] = len(row_with_additional_info) - 2
            row_indices["Total help"] = len(row_with_additional_info) - 1
            
            # Sanity check to see if the indices we added are correct
            assert row_with_additional_info[row_indices["1B network time"]] == str(network_netinst_time) or row_with_additional_info[row_indices["1B network time"]] == "N/A"

            results.append(row_with_additional_info)

    data_1 = [int(row[row_indices["Netinst rating"]]) for row in results if row[row_indices["Netinst rating"]].isnumeric()]
    data_2 = [int(row[row_indices["Calamares rating"]]) for row in results if row[row_indices["Calamares rating"]].isnumeric()]
    print_stats_info(data_1, data_2)
    print(f"  mean netinst:   {mean(data_1)}")
    print(f"  mean calamares: {mean(data_2)}")
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Difficulty rating (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Difficulty rating (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    total_time_no_invalid = [int(row[row_indices["Total valid time"]]) for row in results if row[row_indices["Total valid time"]].isnumeric()]
    total_time = [int(row[row_indices["Total time"]]) for row in results]
    print_stats_info(total_time_no_invalid, total_time, first="Total valid time:", second="Total time:")
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(total_time_no_invalid)
    axs[0].set_title('Total time (valid only)')
    axs[1].boxplot(total_time)
    axs[1].set_title('Total time (all)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    total_help_no_invalid = [int(row[row_indices["Total valid help"]]) for row in results if row[row_indices["Total valid help"]].isnumeric()]
    total_help = [int(row[row_indices["Total help"]]) for row in results]
    print_stats_info(total_help_no_invalid, total_help, first="Total valid help:", second="Total help:")
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(total_help_no_invalid)
    axs[0].set_title('Total help (valid only)')
    axs[1].boxplot(total_help)
    axs[1].set_title('Total help (all)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    total_time_netinst = [int(row[row_indices["1B cumulative time"]]) for row in results if row[row_indices["1B cumulative time"]].isnumeric()]
    total_time_calamares = [int(row[row_indices["2B cumulative time"]]) for row in results if row[row_indices["2B cumulative time"]].isnumeric()]
    print_stats_info(total_time_netinst, total_time_calamares)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(total_time_netinst)
    axs[0].set_title('Total time (netinst)')
    axs[1].boxplot(total_time_calamares)
    axs[1].set_title('Total time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    total_help_netinst = [int(row[row_indices["1B cumulative help"]]) for row in results if row[row_indices["1B cumulative help"]].isnumeric()]
    total_help_calamares = [int(row[row_indices["2B cumulative help"]]) for row in results if row[row_indices["2B cumulative help"]].isnumeric()]
    print_stats_info(total_help_netinst, total_help_calamares)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(total_help_netinst)
    axs[0].set_title('Total help (netinst)')
    axs[1].boxplot(total_help_calamares)
    axs[1].set_title('Total help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    # Equivalence of tasks:
    # Finding iso:                      A.1          && A.2
    # Language, keyboard, location:     1B.2         && 2B.5
    # Configuring network:              1B.(3+4)     && 2B.2
    # Configuring users:                1B.5         && 2B.7
    # Partitioning:                     1B.7         && 2B.6
    # Installing:                       1B.(8+10+11) && 2B.8

    iso_time_netinst = [int(row[row_indices["A.1 time"]]) for row in results if row[row_indices["A.1 time"]].isnumeric()]
    iso_time_calamares = [int(row[row_indices["A.2 time"]]) for row in results if row[row_indices["A.2 time"]].isnumeric()]
    print_stats_info(iso_time_netinst, iso_time_calamares)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(iso_time_netinst)
    axs[0].set_title('Finding iso time (netinst)')
    axs[1].boxplot(iso_time_calamares)
    axs[1].set_title('Finding iso time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    iso_help_netinst = [int(row[row_indices["A.1 help"]]) for row in results if row[row_indices["A.1 help"]].isnumeric()]
    iso_help_calamares = [int(row[row_indices["A.2 help"]]) for row in results if row[row_indices["A.2 help"]].isnumeric()]
    print_stats_info(iso_help_netinst, iso_help_calamares)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(iso_help_netinst)
    axs[0].set_title('Finding iso help (netinst)')
    axs[1].boxplot(iso_help_calamares)
    axs[1].set_title('Finding iso help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B.2 time"]]) for row in results if row[row_indices["1B.2 time"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.5 time"]]) for row in results if row[row_indices["2B.5 time"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Basics time (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Basics time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B.2 help"]]) for row in results if row[row_indices["1B.2 help"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.5 help"]]) for row in results if row[row_indices["2B.5 help"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Basics help (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Basics help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B network time"]]) for row in results if row[row_indices["1B network time"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.2 time"]]) for row in results if row[row_indices["2B.2 time"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Network time (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Network time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B network help"]]) for row in results if row[row_indices["1B network help"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.2 help"]]) for row in results if row[row_indices["2B.2 help"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Network help (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Network help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B.5 time"]]) for row in results if row[row_indices["1B.5 time"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.7 time"]]) for row in results if row[row_indices["2B.7 time"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Users time (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Users time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B.5 help"]]) for row in results if row[row_indices["1B.5 help"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.7 help"]]) for row in results if row[row_indices["2B.7 help"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Users help (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Users help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B.7 time"]]) for row in results if row[row_indices["1B.7 time"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.6 time"]]) for row in results if row[row_indices["2B.6 time"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Partitions time (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Partitions time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B.7 help"]]) for row in results if row[row_indices["1B.7 help"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.6 help"]]) for row in results if row[row_indices["2B.6 help"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Partitions help (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Partitions help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B install time"]]) for row in results if row[row_indices["1B install time"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.8 time"]]) for row in results if row[row_indices["2B.8 time"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Install time (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Install time (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()

    data_1 = [int(row[row_indices["1B install help"]]) for row in results if row[row_indices["1B install help"]].isnumeric()]
    data_2 = [int(row[row_indices["2B.8 help"]]) for row in results if row[row_indices["2B.8 help"]].isnumeric()]
    print_stats_info(data_1, data_2)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    axs[0].boxplot(data_1)
    axs[0].set_title('Install help (netinst)')
    axs[1].boxplot(data_2)
    axs[1].set_title('Install help (Calamares)')
    axs[0].set_ylim(bottom=0)
    axs[1].set_ylim(bottom=0)
    plt.show()


if __name__ == "__main__":
    main()