from sys import argv
from scipy import stats
from scipy.ndimage import median
import csv

# Generates a 2x2 contigency table for 2 equivalent numeric variables under 2 different categories,
# comparing them against the given threshold (over/under, often the median)
def generate_contigency_2num_equivalent(results, numeric_index_1, numeric_index_2, threshold):
    contigency = [[0, 0], [0, 0]]
    for row in results:
        if row[numeric_index_1].isnumeric():
            if int(row[numeric_index_1]) < threshold:
                contigency[0][0] += 1
            else:
                contigency[0][1] += 1

        if row[numeric_index_2].isnumeric():
            if int(row[numeric_index_2]) <= threshold:
                contigency[1][0] += 1
            else:
                contigency[1][1] += 1
    
    return contigency

# Executes a chi-square test with one numeric variable and one categorical variable
# The numeric variable is converted to categorical by creating the categories "Above median" and "Below median"
# `categorical_count`: number of categories in the categorical variable. Categories must be consecutive integers
# `categorical_delta_0index`: delta applied to the int value of the category to make it 0-indexed
def chi_square_1num_1categ(results, numeric_variable_index, categorical_variable_index, categorical_count, categorical_delta_0index = 0):
    numerical_median = median([int(row[numeric_variable_index]) for row in results if row[numeric_variable_index].isnumeric()])

    contigency = [[0, 0] for _ in range(categorical_count)]
    n = 0
    for row in results:
        if row[numeric_variable_index].isnumeric() and row[categorical_variable_index].isnumeric():
            n += 1
            index = int(row[categorical_variable_index]) + categorical_delta_0index
            if int(row[numeric_variable_index]) <= numerical_median:
                contigency[index][0] += 1
            else:
                contigency[index][1] += 1

    return (n, stats.chi2_contingency(contigency, True))

# Same as above, but with 2 categorical data
def chi_square_2categ(results, categ_index_1, categ_index_2, categ_count_1, categ_count_2, categ_delta_0index_1, categ_delta_0index_2):
    contigency = [[0 for i in range(categ_count_1)] for j in range(categ_count_2)]
    n = 0
    for row in results:
        if row[categ_index_1].isnumeric() and row[categ_index_2].isnumeric():
            n += 1
            contigency[int(row[categ_index_2]) + categ_delta_0index_2][int(row[categ_index_1]) + categ_delta_0index_1] += 1

    return (n, stats.chi2_contingency(contigency, True))

# Executes a chi-square test with 2 numerical variables that are equivalent between two categories
# So, each row in results is counted twice to form the contigency table: once for each of the "categories"
# We can do this because EACH row has the data for BOTH categories.
# Then, inside each category, just like above, 2 categories are formed for over/under the median for all data (both categories)

# This is a special case of the Chi-square test called the "Median test"
def chi_square_2num_equivalent(results, numeric_index_1, numeric_index_2):
    numerical_median = median(
        [int(row[numeric_index_1]) for row in results if row[numeric_index_1].isnumeric()] +
        [int(row[numeric_index_2]) for row in results if row[numeric_index_2].isnumeric()]
    )

    contigency = generate_contigency_2num_equivalent(results, numeric_index_1, numeric_index_2, numerical_median)

    n = sum([sum(subarr) for subarr in contigency])

    return (n, stats.chi2_contingency(contigency, True))


def fisher_1categ_1num(results, categ_index, numeric_index, categ_count, categ_delta_0index):
    numerical_median = median([int(row[numeric_index]) for row in results if row[numeric_index].isnumeric()])

    contigency = [[0, 0] for _ in range(categ_count)]
    n = 0
    for row in results:
        if row[numeric_index].isnumeric() and row[categ_index].isnumeric():
            n += 1
            index = int(row[categ_index]) + categ_delta_0index
            if int(row[numeric_index]) <= numerical_median:
                contigency[index][0] += 1
            else:
                contigency[index][1] += 1

    return (n, stats.fisher_exact(contigency))


def fisher_2num_equivalent(results, numeric_index_1, numeric_index_2):
    numerical_median = median(
        [int(row[numeric_index_1]) for row in results if row[numeric_index_1].isnumeric()] +
        [int(row[numeric_index_2]) for row in results if row[numeric_index_2].isnumeric()]
    )

    contigency = generate_contigency_2num_equivalent(results, numeric_index_1, numeric_index_2, numerical_median)

    n = sum([sum(subarr) for subarr in contigency])

    return (n, stats.fisher_exact(contigency))


def mh_chi2_1ord_1num(results, ordinal_index, numeric_index):
    n = 0
    xs = []
    ys = []
    for row in results:
        if row[ordinal_index].isnumeric() and row[numeric_index].isnumeric():
            n += 1
            xs.append(int(row[ordinal_index]))
            ys.append(int(row[numeric_index]))

    r = stats.pearsonr(xs, ys).statistic
    print(f'r: {r}')

    m = (n - 1) * r * r

    return (n, stats.chi2.sf(m, df=1))


def mh_chi2_1ord_1categ(results, ordinal_index, categ_index):
    return mh_chi2_1ord_1num(results, ordinal_index, categ_index)


def execute_part2_test(results, ordinal_index, other_index, other_is_numeric, chi2_other_args):
    if other_is_numeric:
        a, b = chi2_other_args
        try:
            n, chi = chi_square_1num_1categ(results, other_index, ordinal_index, a, b)
            print(f"  n={n}")
            print(f"  p={chi.pvalue}")
        except:
            print(f"FAILED.")
    else:
        a, b, c, d = chi2_other_args
        n, chi = chi_square_2categ(results, ordinal_index, other_index, a, b, c, d)
        print(f"  n={n}")
        print(f"  p={chi.pvalue}")
    print("...using Mantel-Haenszel Chi square")
    n, p = mh_chi2_1ord_1categ(results, ordinal_index, other_index)
    print(f"  n={n}")
    print(f"  p={p}")

# Equivalence of tasks:
# Finding iso:                      A.1          && A.2
# Language, keyboard, location:     1B.2         && 2B.5
# Configuring network:              1B.(3+4)     && 2B.2
# Configuring users:                1B.5         && 2B.7
# Partitioning:                     1B.7         && 2B.6
# Installing:                       1B.(8+10+11) && 2B.8

# Whole installation process:   A.(1+3+4) + 1B.* && A.(2+3+4) + 2B.*

# Pretty ugly but I'm tired
COLUMNS_FOR_NETINST_CUMULATIVE = [
    "A.1", "A.3", "A.4",
    "1B.1", "1B.2", "1B.3", "1B.4", "1B.5", "1B.6",
    "1B.7", "1B.8", "1B.9", "1B.10", "1B.11", "1B.12",
    "1B.13", "1B.14",
]

COLUMNS_FOR_CALAMARES_CUMULATIVE = [
    "A.2", "A.3", "A.4",
    "2B.1", "2B.2", "2B.3", "2B.4", "2B.5", "2B.6",
    "2B.7", "2B.8", "2B.9", "2B.10",
]

def main():
    if len(argv) != 2:
        print("Incorrect usage")
        print("Correct usage: python stats.py [results_csv]")
        exit(1)

    results_path = argv[1]
    results = []
    row_indices = {}

    with open(results_path, newline="") as results_file:
        parser = csv.reader(results_file, dialect="unix")

        # Parse first line of results (column names)
        column_names = parser.__next__()
        for index, column_name in enumerate(column_names):
            row_indices[column_name] = index

        # Get all the data
        for row in parser:
            # We need these "cumulative" info (for each of time and help):
            # The wildcard (*) is replaced by "time" and "help"
            # key "1B cumulative *"     ==> A.(1+3+4) + 1B.*
            # key "2B cumulative *"     ==> A.(2+3+4) + 2B.*
            # key "1B network *"        ==> 1B.(3+4)
            # key "1B install *"        ==> 1B.(8+10+11)

            total_netinst_time = 0
            total_calamares_time = 0
            total_netinst_help = 0
            total_calamares_help = 0
            total_netinst_is_valid = True
            total_calamares_is_valid = True

            for task in COLUMNS_FOR_NETINST_CUMULATIVE:
                time_column_name = task + " time"
                help_column_name = task + " help"
                if row[row_indices[time_column_name]].isnumeric() and row[row_indices[help_column_name]].isnumeric():
                    total_netinst_time += int(row[row_indices[time_column_name]])
                    total_netinst_help += int(row[row_indices[help_column_name]])
                else:
                    total_netinst_is_valid = False
                    break

            for task in COLUMNS_FOR_CALAMARES_CUMULATIVE:
                time_column_name = task + " time"
                help_column_name = task + " help"
                try:
                    total_calamares_time += int(row[row_indices[time_column_name]])
                    total_calamares_help += int(row[row_indices[help_column_name]])
                except:
                    total_calamares_is_valid = False

            network_netinst_time = 0
            network_netinst_help = 0
            network_netinst_is_valid = True

            try:
                network_netinst_time += int(row[row_indices["1B.3 time"]])
                network_netinst_time += int(row[row_indices["1B.4 time"]])
                network_netinst_help += int(row[row_indices["1B.3 help"]])
                network_netinst_help += int(row[row_indices["1B.4 help"]])
            except:
                network_netinst_is_valid = False

            install_netinst_time = 0
            install_netinst_help = 0
            install_netinst_is_valid = True
            
            try:
                install_netinst_time += int(row[row_indices["1B.8 time"]])
                install_netinst_time += int(row[row_indices["1B.10 time"]])
                install_netinst_time += int(row[row_indices["1B.11 time"]])
                install_netinst_help += int(row[row_indices["1B.8 help"]])
                install_netinst_help += int(row[row_indices["1B.10 help"]])
                install_netinst_help += int(row[row_indices["1B.11 help"]])
            except:
                install_netinst_is_valid = False

            row_with_additional_info = row
            
            # We convert the numbers back to strings simply because all variables are strings
            row_with_additional_info.append(str(total_netinst_time) if total_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(total_netinst_help) if total_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(total_calamares_time) if total_calamares_is_valid else "N/A")
            row_with_additional_info.append(str(total_calamares_help) if total_calamares_is_valid else "N/A")
            row_with_additional_info.append(str(network_netinst_time) if network_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(network_netinst_help) if network_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(install_netinst_time) if install_netinst_is_valid else "N/A")
            row_with_additional_info.append(str(install_netinst_help) if install_netinst_is_valid else "N/A")

            row_indices["1B cumulative time"] = len(row_with_additional_info) - 8
            row_indices["1B cumulative help"] = len(row_with_additional_info) - 7
            row_indices["2B cumulative time"] = len(row_with_additional_info) - 6
            row_indices["2B cumulative help"] = len(row_with_additional_info) - 5
            row_indices["1B network time"] = len(row_with_additional_info) - 4
            row_indices["1B network help"] = len(row_with_additional_info) - 3
            row_indices["1B install time"] = len(row_with_additional_info) - 2
            row_indices["1B install help"] = len(row_with_additional_info) - 1
            
            # Sanity check to see if the indices we added are correct
            assert row_with_additional_info[row_indices["1B network time"]] == str(network_netinst_time) or row_with_additional_info[row_indices["1B network time"]] == "N/A"

            results.append(row_with_additional_info)

    # Part 1: Relationship between the installer used (Calamares or Regular) and OD
    print("PART 1 --- RELATIONSHIP BETWEEN THE INSTALLER USED AND O.D.")

    # 1.1: Using difficulty rating as OD
    # Contigency table:
    #     Regular     Calamares
    # 1
    # 2
    # 3
    # 4
    # 5

    n_inst_dr = 0
    contigency = [[0, 0] for _ in range(5)]
    for row in results:
        contigency[-1 + int(row[row_indices["Netinst rating"]])][0] += 1
        contigency[-1 + int(row[row_indices["Calamares rating"]])][1] += 1
        n_inst_dr += 2

    chi2_inst_dr = stats.chi2_contingency(contigency, True)
    print("1.1: Installer used && difficulty rating --- Chi square test")
    print(f"  n={n_inst_dr}")
    print(f"  p={chi2_inst_dr.pvalue}\n")

    # Using Mantel-Haenszel Chi square
    n_inst_dr_mh = 0
    xs_inst_dr = []
    ys_inst_dr = []

    for row in results:
        n_inst_dr_mh += 2
        xs_inst_dr.append(0)
        xs_inst_dr.append(1)
        ys_inst_dr.append(int(row[row_indices["Netinst rating"]]))
        ys_inst_dr.append(int(row[row_indices["Calamares rating"]]))

    r_inst_dr = stats.pearsonr(xs_inst_dr, ys_inst_dr).statistic
    m_inst_dr = (n_inst_dr_mh - 1) * r_inst_dr * r_inst_dr

    print(f"...using Mantel-Haenszel Chi square")
    print(f"  n={n_inst_dr_mh}")
    print(f"  p={stats.chi2.sf(m_inst_dr, df=1)}")


    # 1.2: Using total time as OD
    # Contigency table:
    #               Regular     Calamares
    # Below median
    # Above median

    # 1.2.1: Cumulative process (part A but only for the relevant iso + part B)
    (n_inst_help_cumulative, chi2_inst_help_cumulative) = chi_square_2num_equivalent(results, row_indices["1B cumulative time"], row_indices["2B cumulative time"])
    print("1.2.1: Installer used && time taken (cumulative) --- Median test")
    print(f"  n={n_inst_help_cumulative}")
    print(f"  p={chi2_inst_help_cumulative.pvalue}\n")

    # 1.2.2: Finding the iso (A.1 && A.2)
    (n_inst_time_iso, chi2_inst_time_iso) = chi_square_2num_equivalent(results, row_indices["A.1 time"], row_indices["A.2 time"])
    print("1.2.2: Installer used && time taken (finding the iso) --- Median test")
    print(f"  n={n_inst_time_iso}")
    print(f"  p={chi2_inst_time_iso.pvalue}\n")

    # 1.2.3: Language, keyboard, location (1B.2 && 2B.5)
    (n_inst_time_basics, chi2_inst_time_basics) = chi_square_2num_equivalent(results, row_indices["1B.2 time"], row_indices["2B.5 time"])
    print("1.2.3: Installer used && time taken (language, keyboard, location) --- Median test")
    print(f"  n={n_inst_time_basics}")
    print(f"  p={chi2_inst_time_basics.pvalue}\n")

    # 1.2.4: Configuring network (1B.(3+4) && 2B.2)
    (n_inst_time_network, chi2_inst_time_network) = chi_square_2num_equivalent(results, row_indices["1B network time"], row_indices["2B.2 time"])
    print("1.2.4: Installer used && time taken (configuring network) --- Median test")
    print(f"  n={n_inst_time_network}")
    print(f"  p={chi2_inst_time_network.pvalue}\n")

    # 1.2.5: Configuring users (1B.5 && 2B.7)
    (n_inst_time_users, chi2_inst_time_users) = chi_square_2num_equivalent(results, row_indices["1B.5 time"], row_indices["2B.7 time"])
    print("1.2.5: Installer used && time taken (configuring users) --- Median test")
    print(f"  n={n_inst_time_users}")
    print(f"  p={chi2_inst_time_users.pvalue}\n")
    
    # 1.2.6: Partitioning (1B.7 && 2B.6)
    (n_inst_time_partitioning, chi2_inst_time_partitioning) = chi_square_2num_equivalent(results, row_indices["1B.7 time"], row_indices["2B.6 time"])
    print("1.2.6: Installer used && time taken (partitioning) --- Median test")
    print(f"  n={n_inst_time_partitioning}")
    print(f"  p={chi2_inst_time_partitioning.pvalue}\n")
    
    # 1.2.7: Installing (1B.(8+10+11) && 2B.8)
    (n_inst_time_install, chi2_inst_time_install) = chi_square_2num_equivalent(results, row_indices["1B install time"], row_indices["2B.8 time"])
    print("1.2.7: Installer used && time taken (installing) --- Median test")
    print(f"  n={n_inst_time_install}")
    print(f"  p={chi2_inst_time_install.pvalue}\n")


    # 1.3: Using total help as OD
    # Contigency table:
    #    Regular     Calamares
    # 1
    # 2
    # 3
    # 4
    # 5

    # 1.3.1: Cumulative process (part A but only for the relevant iso + part B)
    (n_inst_help_cumulative, fisher_inst_help_cumulative) = fisher_2num_equivalent(results, row_indices["1B cumulative help"], row_indices["2B cumulative help"])
    print("1.3.1: Installer used && help needed (cumulative) --- Fisher's exact test")
    print(f"  n={n_inst_help_cumulative}")
    print(f"  p={fisher_inst_help_cumulative.pvalue}\n")

    # 1.3.2: Finding the iso (A.1 && A.2)
    (n_inst_help_iso, fisher_inst_help_iso) = fisher_2num_equivalent(results, row_indices["A.1 help"], row_indices["A.2 help"])
    print("1.3.2: Installer used && help needed (finding the iso) --- Fisher's exact test")
    print(f"  n={n_inst_help_iso}")
    print(f"  p={fisher_inst_help_iso.pvalue}\n")

    # 1.3.3: Language, keyboard, location (1B.2 && 2B.5)
    (n_inst_help_basics, fisher_inst_help_basics) = fisher_2num_equivalent(results, row_indices["1B.2 help"], row_indices["2B.5 help"])
    print("1.3.3: Installer used && help needed (language, keyboard, location) --- Fisher's exact test")
    print(f"  n={n_inst_help_basics}")
    print(f"  p={fisher_inst_help_basics.pvalue}\n")

    # 1.3.4: Configuring network (1B.(3+4) && 2B.2)
    (n_inst_help_network, fisher_inst_help_network) = fisher_2num_equivalent(results, row_indices["1B network help"], row_indices["2B.2 help"])
    print("1.3.4: Installer used && help needed (configuring network) --- Fisher's exact test")
    print(f"  n={n_inst_help_network}")
    print(f"  p={fisher_inst_help_network.pvalue}\n")

    # 1.3.5: Configuring users (1B.5 && 2B.7)
    (n_inst_help_users, fisher_inst_help_users) = fisher_2num_equivalent(results, row_indices["1B.5 help"], row_indices["2B.7 help"])
    print("1.3.5: Installer used && help needed (configuring users) --- Fisher's exact test")
    print(f"  n={n_inst_help_users}")
    print(f"  p={fisher_inst_help_users.pvalue}\n")
    
    # 1.3.6: Partitioning (1B.7 && 2B.6)
    (n_inst_help_partitioning, chi2_inst_help_partitioning) = chi_square_2num_equivalent(results, row_indices["1B.7 help"], row_indices["2B.6 help"])
    print("1.3.6: Installer used && help needed (partitioning) --- Fisher's exact test")
    print(f"  n={n_inst_help_partitioning}")
    print(f"  p={chi2_inst_help_partitioning.pvalue}\n")
    
    # 1.3.7: Installing (1B.(8+10+11) && 2B.8)
    (n_inst_help_install, fisher_inst_help_install) = fisher_2num_equivalent(results, row_indices["1B install help"], row_indices["2B.8 help"])
    print("1.3.7: Installer used && help needed (installing) --- Fisher's exact test")
    print(f"  n={n_inst_help_install}")
    print(f"  p={fisher_inst_help_install.pvalue}\n")


    # Part 2: Relationship between technical ability and netinst OD
    print("PART 2 --- RELATIONSHIP BETWEEN TECHNICAL ABILITY AND NETINST/CALAMARES O.D.")
    print("2.1.1: Netinst rating && technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Netinst rating"], row_indices["Technical ability"], False, (5, 3, -1, -1))

    print("2.1.2: Netinst rating && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["Netinst rating"], row_indices["OS experience"], False, (5, 3, -1, -1))

    print("2.1.3: Netinst rating && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Netinst rating"], row_indices["Linux experience"], False, (5, 3, -1, -1))

    print("2.2.1: Calamares rating && technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Calamares rating"], row_indices["Technical ability"], False, (4, 3, -1, -1))

    print("2.2.2: Calamares rating && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["Calamares rating"], row_indices["OS experience"], False, (4, 3, -1, -1))

    print("2.2.3: Calamares rating && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Calamares rating"], row_indices["Linux experience"], False, (4, 3, -1, -1))

    # ------------------------------------------

    print("2.3.1: Netinst time taken (cumulative) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["1B cumulative time"], True, (3, -1))
    print("2.3.2: Netinst time taken (finding the iso) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["A.1 time"], True, (3, -1))
    print("2.3.3: Netinst time taken (language, keyboard, location) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["1B.2 time"], True, (3, -1))
    print("2.3.4: Netinst time taken (configuring network) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["1B network time"], True, (3, -1))
    print("2.3.5: Netinst time taken (configuring users) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["1B.5 time"], True, (3, -1))
    print("2.3.6: Netinst time taken (partitioning) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["1B.7 time"], True, (3, -1))
    print("2.3.7: Netinst time taken (installing) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["1B install time"], True, (3, -1))


    print("2.4.1: Netinst time taken (cumulative) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["1B cumulative time"], True, (3, -1))
    print("2.4.2: Netinst time taken (finding the iso) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["A.1 time"], True, (3, -1))
    print("2.4.3: Netinst time taken (language, keyboard, location) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["1B.2 time"], True, (3, -1))
    print("2.4.4: Netinst time taken (configuring network) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["1B network time"], True, (3, -1))
    print("2.4.5: Netinst time taken (configuring users) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["1B.5 time"], True, (3, -1))
    print("2.4.6: Netinst time taken (partitioning) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["1B.7 time"], True, (3, -1))
    print("2.4.7: Netinst time taken (installing) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["1B install time"], True, (3, -1))


    print("2.5.1: Netinst time taken (cumulative) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["1B cumulative time"], True, (3, -1))
    print("2.5.2: Netinst time taken (finding the iso) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["A.1 time"], True, (3, -1))
    print("2.5.3: Netinst time taken (language, keyboard, location) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["1B.2 time"], True, (3, -1))
    print("2.5.4: Netinst time taken (configuring network) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["1B network time"], True, (3, -1))
    print("2.5.5: Netinst time taken (configuring users) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["1B.5 time"], True, (3, -1))
    print("2.5.6: Netinst time taken (partitioning) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["1B.7 time"], True, (3, -1))
    print("2.5.7: Netinst time taken (installing) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["1B install time"], True, (3, -1))

    print("2.6.1: Calamares time taken (cumulative) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["2B cumulative time"], True, (3, -1))
    print("2.6.2: Calamares time taken (finding the iso) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["A.2 time"], True, (3, -1))
    print("2.6.3: Calamares time taken (language, keyboard, location) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["2B.5 time"], True, (3, -1))
    print("2.6.4: Calamares time taken (configuring network) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["2B.2 time"], True, (3, -1))
    print("2.6.5: Calamares time taken (configuring users) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["2B.7 time"], True, (3, -1))
    print("2.6.6: Calamares time taken (partitioning) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["2B.6 time"], True, (3, -1))
    print("2.6.7: Calamares time taken (installing) && Technical ability --- Chi square test")
    execute_part2_test(results, row_indices["Technical ability"], row_indices["2B.8 time"], True, (3, -1))


    print("2.7.1: Calamares time taken (cumulative) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["2B cumulative time"], True, (3, -1))
    print("2.7.2: Calamares time taken (finding the iso) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["A.2 time"], True, (3, -1))
    print("2.7.3: Calamares time taken (language, keyboard, location) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["2B.5 time"], True, (3, -1))
    print("2.7.4: Calamares time taken (configuring network) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["2B.2 time"], True, (3, -1))
    print("2.7.5: Calamares time taken (configuring users) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["2B.7 time"], True, (3, -1))
    print("2.7.6: Calamares time taken (partitioning) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["2B.6 time"], True, (3, -1))
    print("2.7.7: Calamares time taken (installing) && OS experience --- Chi square test")
    execute_part2_test(results, row_indices["OS experience"], row_indices["2B.8 time"], True, (3, -1))


    print("2.8.1: Calamares time taken (cumulative) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["2B cumulative time"], True, (3, -1))
    print("2.8.2: Calamares time taken (finding the iso) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["A.2 time"], True, (3, -1))
    print("2.8.3: Calamares time taken (language, keyboard, location) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["2B.5 time"], True, (3, -1))
    print("2.8.4: Calamares time taken (configuring network) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["2B.2 time"], True, (3, -1))
    print("2.8.5: Calamares time taken (configuring users) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["2B.7 time"], True, (3, -1))
    print("2.8.6: Calamares time taken (partitioning) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["2B.6 time"], True, (3, -1))
    print("2.8.7: Calamares time taken (installing) && Linux experience --- Chi square test")
    execute_part2_test(results, row_indices["Linux experience"], row_indices["2B.8 time"], True, (3, -1))

if __name__ == "__main__":
    main()