import os
from sys import argv
import csv

TASK_NAMES = [
    "A.1: Downloading Debian image",
    "A.2: Downloading live image",
    "A.3: Flashing",
    "A.4: Booting from USB",
    "1B.1: Regular installar: booting and selecting \"graphical install\"",
    "1B.2: Configuring language, location, keyboard",
    "1B.3: Network configuration",
    "1B.4: Hostname, domain name",
    "1B.5: Setting up users and passwords",
    "1B.6: Clock configuration",
    "1B.7: Disk partitioning",
    "1B.8: Package manager and mirrors configuration",
    "1B.9: Option to participate in popularity contest",
    "1B.10: Installing software",
    "1B.11: Installing bootloader",
    "1B.12: Rebooting into Debian",
    "1B.13: Logging in",
    "1B.14: Exploring the system",
    "2B.1: Calamares: booting and selecting \"live system\"",
    "2B.2: General configuration and WiFi",
    "2B.3: Exploring the system",
    "2B.4: Launching the Calamares installer",
    "2B.5: Configuring language, location, keyboard",
    "2B.6: Configuring partitions",
    "2B.7: Configuring users and passwords",
    "2B.8: Installing the system",
    "2B.9: Rebooting into Debian",
    "2B.10: Logging in"
]

# Parses a single task file (according to the layout in ./observer-notes.md)
def parseSingleFile(path):
    tester_number = "Unknown"
    calamares_first = "?"

    # When parsing tasks, is equal to the current part of a task we are parsing
    # Used to correctly parse multi-line data
    # Possible values are "none" (not yet at the "Tasks" section), "time", "help", "notes"
    parsing_stage = "none"
    task_data = []

    with open(path, 'r') as in_file:
        for line_number, line in enumerate(in_file):
            # Parse "General information" section
            if "- Tester #:" in line:
                tester_number = line.replace("- Tester #:", "").strip()
                continue
            if "- Regular or Calamares first:" in line:
                if "calamares" in line.replace("- Regular or Calamares first:", "").lower():
                    calamares_first = "1"
                else:
                    calamares_first = "0"
                continue

            # Task subsections start with "###" and are the only lines that begin with this string
            if line[0:3] == "###":
                parsing_stage = "time"
                task_data.append({
                    # Only keep the task number (before the colon, for example "1B.3")
                    "name": TASK_NAMES[len(task_data)].split(":")[0],
                    "time": "Unknown",
                    "help": "Unknown",
                    "notes": "Unknown"
                })
                continue

            # Parse the first line of a *possibly* multiline subsection of a task (time taken, help needed, or other notes)
            if "- Time taken:" in line:
                parsing_stage = "time"
                time_taken = line.replace("- Time taken:", "").strip()

                if "N/A" in time_taken:
                    print(f"Warning: During parsing {path}, at line {line_number + 1}: skipping time taken. N/A was found.")
                    task_data[-1][parsing_stage] = "N/A"
                    continue

                time_taken = time_taken.split(":")

                # Make sure there's 2 parts (minutes, seconds) and that it's parsable as an int (format ##:##)
                assert len(time_taken) == 2, f"Fatal error during parsing {path}, at line {line_number + 1}: time taken does not have 2 parts in format minutes:seconds"
                assert time_taken[0].isnumeric() and time_taken[1].isnumeric(), f"Fatal error during parsing {path}, at line {line_number + 1}: time taken is not numeric"
                task_data[-1][parsing_stage] = int(time_taken[0]) * 60 + int(time_taken[1])
                continue

            if "- Help needed (verbose/hint/tutorial):" in line:
                parsing_stage = "help"
                help_needed = line.replace("- Help needed (verbose/hint/tutorial):", "").strip().lower()

                if "n/a" in help_needed:
                    print(f"Warning: During parsing {path}, at line {line_number + 1}: skipping help needed. N/A was found.")
                    task_data[-1][parsing_stage] = "N/A"
                    continue

                if "none" in help_needed:
                    task_data[-1][parsing_stage] = 0
                    continue
                elif "tutorial" in help_needed:
                    task_data[-1][parsing_stage] = 3
                    continue
                elif "verbose" in help_needed:
                    task_data[-1][parsing_stage] = 2
                    continue
                elif "hint" in help_needed:
                    task_data[-1][parsing_stage] = 1
                    continue
                
                assert False, f"Fatal error during parsing {path}, at line {line_number + 1}: help needed does not contain 'none', 'verbose', 'hint', or 'tutorial'"

            if "- Other notes and struggles:" in line:
                parsing_stage = "notes"
                task_data[-1][parsing_stage] = line.replace("- Other notes and struggles:", "").strip()
                continue


            # If we haven't started to parse tasks (still in "General" section), nothing to do
            if parsing_stage == "none":
                continue

            # If none of the above occured, then 2 possibilities:
            # - We are inside a multi-line task result, so we need to add this line to the current task we're parsing
            # OR
            # - We are at an irrelevant line: either all whitespace or a bunch of underscores. To test for this, we will check if there are any alphanumeric characters

            # In the first case, if it's a bunch of underscores AND we have the correct number of tasks, then the whole thing is finished
            # TODO handle comments after the last bunch of underscores
            
            if "_________" in line and len(task_data) == len(TASK_NAMES):
                # Finished all tasks, so finished parsing
                break

            if not any(c.isalnum() for c in line):
                # Irrelevant line, skip
                continue

            task_data[-1][parsing_stage] += "\\n" + line.strip().strip("- ")

    return {
        "task_data": task_data,
        "tester_number": tester_number,
        "calamares_first": calamares_first,
    }

def parseDemographicsData(demographics_data):
    for tester in demographics_data:
        # Parsing the field "tech_ability"
        if "Beginner" in tester["tech_ability"]:
            tester["tech_ability_numeric"] = 1
        elif "Intermediate" in tester["tech_ability"]:
            tester["tech_ability_numeric"] = 2
        elif "Advanced" in tester["tech_ability"]:
            tester["tech_ability_numeric"] = 3
        else:
            tester["tech_ability_numeric"] = 0

        # Parsing the field "preferred_os"
        # Handles multiple responses per tester
        preferred_os_parsed = []
        if "windows" in tester["preferred_os"].lower():
            preferred_os_parsed.append("WINDOWS")
        if "mac" in tester["preferred_os"].lower():
            preferred_os_parsed.append("MACOS")
        if "linux" in tester["preferred_os"].lower():
            preferred_os_parsed.append("LINUX")

        if len(preferred_os_parsed) == 0:
            tester["preferred_os_parsed"] = "UNKNOWN"
        else:
            tester["preferred_os_parsed"] = " ".join(preferred_os_parsed)

        # Parsing the field "os_experience"
        if "None" in tester["os_experience"]:
            tester["os_experience_numeric"] = 1
        elif "Some" in tester["os_experience"]:
            tester["os_experience_numeric"] = 2
        elif "A lot" in tester["os_experience"]:
            tester["os_experience_numeric"] = 3
        else:
            tester["os_experience_numeric"] = 0

        # Parsing the field "linux_experience"
        if "None" in tester["linux_experience"]:
            tester["linux_experience_numeric"] = 1
        elif "Some" in tester["linux_experience"]:
            tester["linux_experience_numeric"] = 2
        elif "A lot" in tester["linux_experience"]:
            tester["linux_experience_numeric"] = 3
        else:
            tester["linux_experience_numeric"] = 0

    return demographics_data

def parseFeedbackData(feedback_data):
    for tester in feedback_data:
        # Parsing the field "netinst_rating"
        if "fast and simple" in tester["netinst_rating"]:
            tester["netinst_rating_numeric"] = 1
        elif "okay" in tester["netinst_rating"]:
            tester["netinst_rating_numeric"] = 2
        elif "minor problems" in tester["netinst_rating"]:
            tester["netinst_rating_numeric"] = 3
        elif "some of the time" in tester["netinst_rating"]:
            tester["netinst_rating_numeric"] = 4
        elif "most of the time" in tester["netinst_rating"]:
            tester["netinst_rating_numeric"] = 5
        else:
            tester["netinst_rating_numeric"] = 0

        # Parsing the field "calamares_rating"
        if "fast and simple" in tester["calamares_rating"]:
            tester["calamares_rating_numeric"] = 1
        elif "okay" in tester["calamares_rating"]:
            tester["calamares_rating_numeric"] = 2
        elif "minor problems" in tester["calamares_rating"]:
            tester["calamares_rating_numeric"] = 3
        elif "some of the time" in tester["calamares_rating"]:
            tester["calamares_rating_numeric"] = 4
        elif "most of the time" in tester["calamares_rating"]:
            tester["calamares_rating_numeric"] = 5
        else:
            tester["calamares_rating_numeric"] = 0
    return feedback_data

def main():
    if len(argv) != 4:
        print("Incorrect usage.")
        print("Correct usage: python parser.py [task_directory] [demographics] [feedback]")
        exit(1)

    task_directory_path = argv[1]
    demographics_csv_path = argv[2]
    feedback_csv_path = argv[3]

    tasks_data = []
    demographics_data = []
    feedback_data = []

    # Task data parsing
    for file in os.listdir(task_directory_path):
        if file.endswith(".md"):
            tasks_data.append(parseSingleFile(os.path.join(task_directory_path, file)))
    
    # Demographics form parsing
    with open(demographics_csv_path, newline="") as demographics_file:
        demographics_parser = csv.reader(demographics_file, dialect="unix")
        for row in demographics_parser:
            demographics_data.append({
                # "timestamp": row[0],
                "tester_number": row[1],
                # "age": row[2],
                # "gender": row[3],
                # "language": row[4],
                # "occupation": row[5],
                "tech_ability": row[6],
                "preferred_os": row[7],
                "os_experience": row[8],
                "linux_experience": row[9],
            })

        # Parse again some of the fields of the form (multiple answer question to numerical values)
        parseDemographicsData(demographics_data)

    # Feedback form parsing
    with open(feedback_csv_path, newline="") as feedback_file:
        feedback_parser = csv.reader(feedback_file, dialect="unix")
        for row in feedback_parser:
            feedback_data.append({
                # "timestamp": row[0],
                "tester_number": row[1],
                # "words_netinst": row[2],
                "netinst_rating": row[3],
                # "words_calamares": row[4],
                "calamares_rating": row[5],
                # "additional_feedback": row[6],
                # "share_name_consent": row[7],
                # "found_out_about_study": row[8],
            })
        
        # Parse again some of the fields of the form (multiple answer question to numerical values)
        parseFeedbackData(feedback_data)

    out_file_name = "results.csv"
    with open(out_file_name, "w+") as out_file:
        csv_writer = csv.writer(out_file)
        # Write header with column names
        csv_writer.writerow([
            "Tester number", "Calamares first", # General information
            "Technical ability", "Preferred OS", "OS experience", "Linux experience", # Demographics form data
            "Netinst rating", "Calamares rating", # Feedback form data
        ]
            + [task.split(":")[0] + " time" for task in TASK_NAMES] # Time needed for all tasks
            + [task.split(":")[0] + " help" for task in TASK_NAMES] # Help needed for all tasks
        )

        for tester_tasks_data in tasks_data:
            tester_number = tester_tasks_data["tester_number"]

            # Finds the first element in demographics_data and feedback_data that matches the tester number
            tester_demographics_data = next(filter(lambda data: data["tester_number"] == tester_number, demographics_data))
            tester_feedback_data = next(filter(lambda data: data["tester_number"] == tester_number, feedback_data))

            # Sanity check: make sure all tasks are ordered correctly
            assert ([task["name"] for task in tester_tasks_data["task_data"]] == [task_name.split(":")[0] for task_name in TASK_NAMES])
            
            tasks_times = [task["time"] for task in tester_tasks_data["task_data"]]
            tasks_help = [task["help"] for task in tester_tasks_data["task_data"]]


            csv_writer.writerow([
                tester_number, tester_tasks_data["calamares_first"], # General information
                tester_demographics_data["tech_ability_numeric"], tester_demographics_data["preferred_os_parsed"], tester_demographics_data["os_experience_numeric"], tester_demographics_data["linux_experience_numeric"], # Feedback form data
                tester_feedback_data["netinst_rating_numeric"], tester_feedback_data["calamares_rating_numeric"], # Feedback form data
            ] + tasks_times + tasks_help)

if __name__ == "__main__":
    main()