# General notes from testing sessions

### Known issues

- since those test machines are old, the hw clock might not be holding the correct time, and that might cause problems. If the network was successfully configured and the step "configuring package manager" fails with no apparent reason, it might be that the wrong time is cause the handshake with the mirror to fail. The solution is to update the BIOS clock before proceeding to installation. Alternatively, proceed with the minimal installation, and once the system is up, set the date and time, then add a mirror to sources.list, and install gnome.

### Improvements to task list

- include tasks with no intervention to our list, so that we can properly take
  the time - separatley from other tasks where the tester actually is performing
a task
- it seems we don't have a portable version for balena anymore, check if we need
  to update hint

### Questions raised by testers, investigate

- after reboot, why DI is not called again if we leave the usb plugged?  with
  the live image, the usb plugged will always cause a way back
- calamares installer: at 70% of installation we see "removing packages" - what
  types of packages are needed then removed?
- if you remove the usb, will it stop the installation process? (with DI and
  calamares, is there a difference?)

### Follow-up, considering opening bug upstream or issue for us

- create video tutorials for debian install that would list all requirements,
  all needed pieces, and follow along the whole process (eg requirement: you
need this much of usb size)
- gnome usability: difficult to find the reboot button on the desktop (hidden behing the battery icon)

