# Tester notes template

## General information

- Tester #: 104
- Regular or Calamares first: Calamares

## Tasks

### A.1: Downloading Debian image
- Time taken: 0:12
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### A.2: Downloading live image
- Time taken: 1:28
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Searched "live" using the search bar in debian.org, then searched "gnome"
  - Then installed the gnome live image the normal way (Other downloads)

### A.3: Flashing
- Time taken: 3:14
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Searched "Flash USB iso" on Google. Found Rufus and BalenaEtcher, used BalenaEtcher

### A.4: Booting from USB
- Time taken: 1:45
- Help needed (verbose/hint/tutorial): None 
- Other notes and struggles: N/A

___________________________________________________

### 1B.1: Regular installar: booting and selecting "graphical install"
- Time taken: 0:40
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.2: Configuring language, location, keyboard
- Time taken: 0:56
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.3: Network configuration
- Time taken: 6:11
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Didn't know which interface to choose (wireless/wired)
  - Didn't know the network security (WEP vs. WPA)
  - Had some network issues unrelated to the participant, so time is slightly higher

### 1B.4: Hostname, domain name
- Time taken: 1:00
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Had no idea what what these meant, made something up to enter in the text prompt

### 1B.5: Setting up users and passwords
- Time taken: 1:22
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Initially entered an invalid username. The installer went back *2 steps* instead of 1 after throwing the "invalid username" error, making the process even more confusing (tester was brought back to another page, not the one expected/the one that failed)

### 1B.6: Clock configuration
- Time taken: 0:11
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.7: Disk partitioning
- Time taken: 2:16
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Initial reaction: "wtf?"
  - Didn't know options to take (erase all disk, etc.), so guessed.
  - Found the process very confusing

### 1B.8: Package manager and mirrors configuration
- Time taken: 7:52
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Had to idea what was going on, went with defaults

### 1B.9: Option to participate in popularity contest
- Time taken: 0:11
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.10: Installing software
- Time taken: 28:13
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.11: Installing bootloader
- Time taken: 2:00
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Wasn't sure if installing the bootloader was needed/preferred or not

### 1B.12: Rebooting into Debian
- Time taken: 1:36
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.13: Logging in
- Time taken: 0:11
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.14: Exploring the system
- Time taken: 2:21
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

___________________________________________________

### 2B.1: Calamares: booting and selecting "live system"
- Time taken: 0:10
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.2: General configuration and WiFi
- Time taken: 2:10
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.3: Exploring the system
- Time taken: 0:20
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Used the GNOME searchbar to search "wallpaper"

### 2B.4: Launching the Calamares installer
- Time taken: 0:06
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.5: Configuring language, location, keyboard
- Time taken: 0:30
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.6: Configuring partitions
- Time taken: 0:09
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.7: Configuring users and passwords
- Time taken: 0:40
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.8: Installing the system
- Time taken: 14:57
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.9: Rebooting into Debian
- Time taken: N/A (unrecorded)
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.10: Logging in
- Time taken: 0:00
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Checked the "Login automatically after rebooting" while installing, so didn't have to log in

___________________________________________________

Feedback on netinst installer:
    Doesn't feel modern
    Very slow (slow enough that it would make them walk away/reconsider installing Debian)

Feedback on Calamares installer:
    Very quick and "cool", easy to install things
    However, found it unintuitive (too used to Windows)
    Being able to use the OS (e.g. play chess, browse on Firefox) *while* the system is installing is very nice
    Instructions are very easy to follow

Debian with the GNOME DE "feels like a smartphone" (used to Android)