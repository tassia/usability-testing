# Tester notes template

## General information

- Tester #: 109
- Regular or Calamares first: Regular

## Tasks

### A.1: Downloading Debian image
- Time taken: 2:05
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Went on the "News" section at the bottom of the website's homepage
  - Searched "Debian 12 image" on Google
  - This gave the page to the live images
  - Then, went back to debian.org and found the right button

### A.2: Downloading live image
- Time taken: 3:19
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Went in "Other downloads"
  - In "Other downloads", went in "Download and installation image", then back to "Other downloads" to click on "Download mirrors"
  - Then found the correct "Live Gnome" link

### A.3: Flashing
- Time taken: 3:20
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Didn't know what flashing a USB meant
  - Searched on Google "How to flash a USB key", which suggested Rufus

### A.4: Booting from USB
- Time taken: 2:53
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

___________________________________________________

### 1B.1: Regular installar: booting and selecting "graphical install"
- Time taken: 0:34
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Wasn't used to not having a mouse on the boot menu

### 1B.2: Configuring language, location, keyboard
- Time taken: 1:05
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.3: Network configuration
- Time taken: N/A (no WiFi during test)
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.4: Hostname, domain name
- Time taken: N/A (no WiFi during test)
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.5: Setting up users and passwords
- Time taken: 1:11
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.6: Clock configuration
- Time taken: N/A (no WiFi during test)
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.7: Disk partitioning
- Time taken: 2:16
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Didn't know what to select between "All files in one partition" or separating /home in another partition
  - Didn't know what to select at the last "Write changes to disk" screen?

### 1B.8: Package manager and mirrors configuration
- Time taken: 5:22
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.9: Option to participate in popularity contest
- Time taken: 0:14
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.10: Installing software
- Time taken: N/A (1:38, because no DE was installed)
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.11: Installing bootloader
- Time taken: 1:36
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Selected "Enter device manually" at first, but went back to select /dev/sda

### 1B.12: Rebooting into Debian
- Time taken: 0:22
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.13: Logging in
- Time taken: 0:24
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 1B.14: Exploring the system
- Time taken: N/A (since no DE was installed)
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

___________________________________________________

### 2B.1: Calamares: booting and selecting "live system"
- Time taken: 0:34
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.2: General configuration and WiFi
- Time taken: 2:11
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.3: Exploring the system
- Time taken: 1:06
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.4: Launching the Calamares installer
- Time taken: 1:16
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles:
  - Initially didn't see the icon on the taskbar, so searched in the application list

### 2B.5: Configuring language, location, keyboard
- Time taken: 0:21
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.6: Configuring partitions
- Time taken: 0:16
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.7: Configuring users and passwords
- Time taken: 0:31
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.8: Installing the system
- Time taken: 14:35
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

### 2B.9: Rebooting into Debian
- Time taken: 2:35
- Help needed (verbose/hint/tutorial): Hint? (see below)
- Other notes and struggles:
  - Didn't remove USB before rebooting
  - Had to tell the tester to remove USB before powering up the computer

### 2B.10: Logging in
- Time taken: 0:10
- Help needed (verbose/hint/tutorial): None
- Other notes and struggles: N/A

___________________________________________________

Feedback:
    Both installers are very similar and have the same difficulty
    Once you boot to the installer, everything is pretty easy
    However, finding both iso files was hard