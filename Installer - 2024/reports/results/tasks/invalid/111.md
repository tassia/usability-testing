# Tester notes template

## General information

- Tester #: 111
- Regular or Calamares first: Regular

## Tasks

### A.1: Downloading Debian image
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### A.2: Downloading live image
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### A.3: Flashing
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### A.4: Booting from USB
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

___________________________________________________

### 1B.1: Regular installar: booting and selecting "graphical install"
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.2: Configuring language, location, keyboard
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.3: Network configuration
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.4: Hostname, domain name
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.5: Setting up users and passwords
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.6: Clock configuration
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.7: Disk partitioning
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.8: Package manager and mirrors configuration
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.9: Option to participate in popularity contest
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.10: Installing software
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.11: Installing bootloader
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.12: Rebooting into Debian
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.13: Logging in
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 1B.14: Exploring the system
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

___________________________________________________

### 2B.1: Calamares: booting and selecting "live system"
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.2: General configuration and WiFi
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.3: Exploring the system
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.4: Launching the Calamares installer
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.5: Configuring language, location, keyboard
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.6: Configuring partitions
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.7: Configuring users and passwords
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.8: Installing the system
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.9: Rebooting into Debian
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

### 2B.10: Logging in
- Time taken: 
- Help needed (verbose/hint/tutorial): 
- Other notes and struggles: 

___________________________________________________