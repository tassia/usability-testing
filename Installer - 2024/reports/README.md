# Test reports

## Instructions

Before a test, create a copy of [`observer-notes.md`](observer-notes.md) into `./results` with the name `[tester-number].md`. See below for the specifications on how to assign tester numbers.

During a test, fill out [observer-notes.md](observer-notes.md)

**Do not include any personal information (i.e. name) in the results, only the tester #.**

## Assigning tester numbers
To be able to run tests in parallel with no risk of duplicating tester numbers, each observer is assigned a **range of tester numbers**:

- Tassia: 1-100
- Justin: 101-200
- Shahe: 201-300
- Anthony: 301-400
- Giuliana: 401-500

Start will tester #x01, then #x02, etc.

Choose whether your first tester will use netinst or Calamares first. Then, alternate between the two for each tester after the first.

## Mistakes that were made
Two tester numbers 106 were assigned. The 106 from Wednesday June 5 is the "real" 106, while the 106 from Saturday June 8, 11:00-13:00 is called `106-2` OR `107` in `./results`. As such, any reference to tester number 107 refers to 106-2.