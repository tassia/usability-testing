# Debian Installer Usability Presentation (DebConf 2024)

## Outline

- Background, rationale & object of research
- Recruitment of volunteers
- Tests: setup, task lists & observation sessions
- Study results
- Discussion & Suggestions
- Final words & thanks

## Background, rationale & object of research

- Usability = how easy user interfaces are to use
- In our case, how easily and clearly it is for users to install Debian
- First phase of the project focused on the standard installer only
  - As part of our discussion, we compared Debian Installer and documentation
    with other distros (Tails, Ubuntu, Fedora) as well as Calamares
  - All compared distros had more user-friendly processes, in our opinion
  - We managed to get a few suggested changes to the website
  - No apparent change to the Debian Installer
  - Since Calamares installer is already available for Debian, we had the idea
    to ask users to perform the two installations and compare their experience
- Thus, in this second phase, we compared 2 installers:
  - Debian Installer, **netinst ISO** (default downloadable image)
  - Calamares installer, **Debian live image**
- Research questions:
  - How can the Debian installer and/or documentation be improved to give users
    a better experience? (no change from phase 1)
  - Is there a correlation between a user's perspective on Debian and its installation process and which installer was being used?

## Recruitment of volunteers

- Feedback from phase 1: low sample diversity leads to bias
- Goal: attract testers with more varied background (and study programs)
- Challenge: to be able to announce to a wider public, the study had to be
  evaluated by the college Ethics Board, which took a few months to happen
- Strategy: posters on billboard and laptop raffle
- Once we the study was approved, it was almost too late, we had to run
  the tests during exams/vacation period

## Tests

### Setup

- 22 Volunteer testers (compared to 10 in phase 1)
 - 1 for the pilot test, 21 to test for real
- 5 oberservers (one of them being the pilot tester)
- The study was launched during our local event (Montreal Debian & Stuff)
- 2-hour slots allocated per tester
- Equipment:
  - 2 usb keys 32 gb
  - windows laptop for part 1 (pre-installation)
  - lenovo thinkpad for the installation itself 
  - WiFi router
- Testes performed in a room at Vanier college 
![Testing Room](testing_room.jpg)
- Availability of a set of donated computers allowed more tests in parallel

### Task lists

- Phase 1:
  1. Prepare a bootable media with Debian 12
  2. Proceed to the installation
  3. Reboot and explore your new system
- Phase 2:
  - A. Prepare the bootable medias with Debian 12
  - 1B. Debian Installer (netinst image)
  - 2B. Calamares Installer (live image)
  - C. Evaluate your experience

In part A, we ask for the preparation of both bootable medias. In practice, for
the sake of time, we already provided both USBs, but we let the tester perform
all tasks until pressing the button to flash the ISO.

### Observation session

1. Observer greeted the tester, explaned study goals and what the test was about
2. Observer handed a [consent form](/forms/consent-form.md), tester read and signed the form 
3. Observer opened an online [demographics form](/forms/demographics-form.md),
  tester filled that up
4. Observer and tester read together the [instructions for testers](/instructions/instruction-testers.md).
5. Observer opened the [simplified task list](/installation/simplified-task-list.md)
6. Observer instructed the tester to read the simplified task list and make their
  way through it, and that marked the beginning of the observation session
7. We started a timer for each task
8. We sat next to them and observed what they were doing
9. We also asked questions about it to both understand their train of thought but
  to maybe help *them* understand better what they were doing
10. When needed, we had prepared 3 level of helpers for each task: explanations,
  hints and tutorials, all organized in a [helper table](/installation/README.md), for easy access
11. The goal was to let them be as autonomous as possible and only if needed, we would show the above documentation
12. We also allowed the use of Internet searches and watching online video tutorials.
13. We took notes and observed their struggle points and wrote down any feedback they had during the test.
14. We opened an online [feedback form](/forms/feedback-form.md),
  tester filled that up

## Study Results

### Time

- Median time taken to complete the test was 1h23m52s
- In the last round, half the time of the task was needed only for part 1 (prepare bootable media)
- This time, part 1 was much quicker, allowing us to perform 2 installations in a reasonable time

### Testers & Demographics

We tested 21 people, 18 students and 3 professionals. <br>
![Graph - Occupation](demographics/occupation.png)

Most of our testers were below 25 years old with the exception of one, being 41 years old. <br>
![Graph - Age](demographics/age.png)

71% of our testers were male and 29% female. <br>
![Graph - Gender](demographics/gender.png)

We had varying native languages, but most testers were English and French speakers. <br>
![Graph - Language](demographics/language.png)

Most of our testers use Windows on their personal computers, 2 use MacOS, 2 use Linux, and 1 use all of them.
![Graph - Operating System](demographics/os.png)

2/3 of our testers reported to have some familiarity with Linux. <br>
![Graph - Familiarity with Linux](demographics/linux.png)

Most of our testers had little to no prior experience the installation of operating systems.
![Graph - Experience Installing OS](demographics/os-install.png)

When asked about proficiency with computers, 90% self-dentified as intermediate or advanced users, only 10% considered themselves beginners.
![Graph - Computer proficiency](demographics/proficiency.png)

### Heatmap

Here is the [simplified task list](../installation/simplified-task-list.md) for reference.

Heatmap <br>
![Heatmap](heatmap.png)

### Statistical Analysis

- 4 control variables (which installer is used, and 3 measures of self-reported technical skills: general experience, OS installation experience, Linux experience)
- 3 response variables (difficulty rating given to each installer, time taken to complete each task/section, help needed for each task/section)
- Our goal was to find which control variables affected which response variables.
- **Hypothesis testing**: one hypothesis to test for every dependant-independant variable pair
- Significance level of 0.05 (standard)
- Used Python with the scipy.stats library
- Statistical tests used: Chi-square test of independance and variations (Mantel-Haenszel chi square test, Median test), Fisher's exact test

Results of our statistical analysis:

- Netinst was found to be given a **higher difficulty rating** than Calamares (n=36, p=0.0003)
  - Very important result: we know for sure that testers found netinst harder to use
- Netinst was found to have **higher install times** than Calamares (n=31, p=0006)
  - Straightforward result, Calamares is not only easier but also faster
- All comparable tasks except 2 were found to be **faster with Calamares** than with netinst
- However, in the steps of configuring the locale and configuring the network: there wasn't enough evidence to accept the hypothesis
- Participants needed **more help with netinst than with Calamares** in all comparable tasks

- When using netinst, people with a higher technical ability and with more Linux experience **were faster to install** than people with no experience (n=15, p<0.0489)
- However, the tests **were inconclusive** when looking at **the same people using Calamares**
  - Very interesting result!
  - Netinst requires some technical skill to use, but Calamares doesn't seem to.

Fast round of miscellaneous statistics:

Median time to complete the installation
- Netinst: 55:45
- Calamares: 34:51
  - 38% faster overall
![Total time](statistics/total-time.png)

Median time to find the iso
- Netinst: 1:06
- Calamares: 3:37
  - Only task where Calamares is slower
![Iso time](statistics/iso-time.png)

Median time to configure users
- Netinst: 1:13  
- Calamares: 0:31
  - 2.4x faster
![Users time](statistics/users-time.png)

Median time to partition disk
- Netinst: 1:16
- Calamares: 0:15
  - 5x faster
![Partitioning time](statistics/partitions-time.png)

Median time waited for the OS to install
- Netinst: 32:40
- Calamares: 14:10
  - 2.3x faster, or 18.5 minutes less!
![Install time](statistics/install-time.png)

## Discussion & Suggestions

### Comparison with phase 1

- No more checksum and signature verification (biggest pain points in phase 1), since this is not part of the website instructions anymore.
- For demographics, even though this time we recruited volunteers from other programs (diversity goal attained!) and we manage to have more testers, our tester population showed to be more tech savy and have been more exposed to Linux than testers in phase 1.
- Much less research on the web during the installation process, and a much greener heatmap.

### Diving into phase 2 parts

- Part A:
  - all testers were able to download the netinst image with no issue (no surprise!)
  - for the live image, many testers got into a navigation trap: "Try Debian live before installing"
  - there was some difficulty with the process of flashing the image to the USB

- Part 1B:
  - the difference between "graphical install" and "install" was not clear
  - technical concepts that are not common knowledge bring confusion to the process, even if users are able to complete the installation ("green" doesn't necessarily means perfection) 
  - most confusing tasks:
    - Network configuration
    - Hostname domain name
    - Disk partitioning
    - Network mirror

- Part 2B:
  - all testers were able to complete the installation with Calamares with no issue

Finally, we have some suggestion of changes that could improve overall user experience:

### Debian Installer

- Most people tend to skim through text, so make the important text in bold.
- Simplify the installation process by keeping only questions with no obvious default option. Every section could have an "advanced options" button, and a separate image with less defaults could also be provided.
- Clarify the boot menu options: "Graphical Installer" and "Installer" are not obvious choices
- For the graphical installer, desktop environment choice, consider adding "previews" of the different desktop environments to help new users see the difference and make a better informed choice.

### Website & Documentation

- Revamp the Debian website and make it easier for users to find the image they need.
- Adding tutorials or at least links to tools for users to prepare their bootable media. Take into consideration that new users will mostly be using different operating systems.
- Use visuals as much as possible. A good example is the Tails installation process. It contains images and the different steps and then an explanation and different tools for each one.
- Consider creating video tutorials that would describe all requirements, all needed pieces, and follow along the whole process.

## Thank You!

thank you to Debian contributors and sponsors <br>
thank you to our pilot tester, Shahe, who also became an observer in our team<br>
thank you to Eastern Bloc for the donated computers <br>
thank you to Vanier college for the physical and network space <br>
thank you to all observers and testers <br>
thank you to Dana and Greg, our design heros <br>
and thank you for listening!
