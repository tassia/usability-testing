# Instructions for observers

We will start with a pilot test (pilot tester) to ensure all our steps are
clear and concise. Please update this document if you notice anything that must
be improved.

## Test setup

Before starting a test:

- Delete all browser history
- Uninstall all apps installed (BalenaEtcher)
- Delete any files downloaded 
- use command <code>Clear-History</code> on Powershell to delete command history (if used)

During the test:

- Start by handing testers a [consent form](../forms/consent-form.md).
- Then ask to complete a short [demographic questionnaire](https://tinyurl.com/deb-demographics).
At the same time, give the tester their "tester #".
- Read out loud the [testers
  instructions](../instructions/instruction-testers.md) and explain to them what
our goals are.
- Finally, hand out the [simplified task
  list](../installation/simplified-task-list.md) and observe.

**Start a timer to keep track of how much time was taken for each task, and take notes!**

## What to note down during a test session

- The amount of time taken *for each individual task*
  - Reset and start a new timer each time the participant finishes a task or have timer laps
- Any time the tester expresses some difficulty, reads out loud repeatedly in order
  to understand, etc, make a note for the corresponding task
- Make a note of concepts that were not clear to the tester
- For each task, if they need more information, provide them with the corresponding
  verbose explanation.
- If they need help to proceed, they should access the corresponding extra hint.
- Finally, if even hints are not enough, they should access the corresponding tutorial,
  and the observer should help them and assure that the next step can be performed.
- Make a note about how much help was needed (if any) in each step.

Follow the [observer report](../reports/observer-notes.md) as a task list
for observers to take notes and grade the tester.

Finally, once the tests are over, ask the tester to fill our our [feedback form](https://tinyurl.com/debian-testing-feedback).

## Grade Scheme

- Green (4 - easy): The tester was able to perform the task using only the simplified task list.
- Yellow (3 - somewhat difficult): The tester needed the verbose task list to perform the task.
- Orange (2 - difficult): The tester needed hints from us to perform the task.
- Red (1 - very difficult): The tester needed a step-by-step or a tutorial from us to perform the task.

## Beyond a test session

- Statistical Analysis

- Produce heat maps and observations along with recommendations based on the
struggles our testers experienced in our final report of these tests.
