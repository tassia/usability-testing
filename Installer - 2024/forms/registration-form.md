## Registration form

**Description**
The F.L.O.S.S Club at Vanier is performing usability tests on the Debian
GNU/Linux operating system installation. The goal of this study is to see how
easily individuals of varying backgrounds and interests manage the installation,
so we can find points that the process can be improved upon.

Should you register, you'll come to Vanier College at a set time, and be given a list of
instructions to install Debian onto a provided laptop. As an incentive, we are
raffling off a donated laptop to the study participants.

We'd like to emphasize that the tests are designed to improve the system, and as
such we aren't evaluating the skills of the individual. We will welcome anyone
and everyone to participate in this study!

* F.L.O.S.S stands for Free/Libre Open Source Software, and our club focuses on learning to contribute to open source projects

**Email**:___________________________

**Name**:___________________________

**Please check the boxes of dates you are available (Morning - Afternoon - Evening)** \
*June 1st:* [ ] [ ] [ ] \
*June 2nd:* [ ] [ ] [ ] \
*June 3rd:* [ ] [ ] [ ] \
*June 4th:* [ ] [ ] [ ] \
*June 5th:* [ ] [ ] [ ] \
*June 6th:* [ ] [ ] [ ] \
*June 7th:* [ ] [ ] [ ] \
*June 8th:* [ ] [ ] [ ] \
*June 9th:* [ ] [ ] [ ] 
