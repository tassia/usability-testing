## Feedback form

The main purpose of this form is to collect information about the tests conducted and any feedback that might be useful for the analysis.

*To begin please fill out the following*:

**What were your 3 words to describe the process using the Debian 12 installer?**

------------------------------------------------------------

------------------------------------------------------------

------------------------------------------------------------

**Rate the difficulty level for the standard Debian 12 installer**
- [ ] The steps were fast and simple
- [ ] It was okay
- [ ] I only had some minor problems
- [ ] I was confused some of the time
- [ ] I was confused most of the time

**What were your 3 words to describe the process using the Calamares installer?**

------------------------------------------------------------

------------------------------------------------------------

------------------------------------------------------------

**Rate the difficulty level for the Calamares Installer**
- [ ] The steps were fast and simple
- [ ] It was okay
- [ ] I only had some minor problems
- [ ] I was confused some of the time
- [ ] I was confused most of the time

**Any other feedback and/or comments you'd like to share?**

------------------------------------------------------------

------------------------------------------------------------

------------------------------------------------------------

------------------------------------------------------------

------------------------------------------------------------

**Would you like your name to appear in reports, presentations, websites about this study?**
- [ ] Yes
- [ ] No

**How did you find out about this study?**
- [ ] I saw the posters at Vanier
- [ ] Through a friend
- [ ] Through the email sent to Vanier students
- [ ] Discord server
- [ ] Other: _______________________________________________

