# Simplified Tasks for Installation of Debian 12 (Green)

## Before you start

There might be many concepts you do not fully understand in the installation
process, and that is totally fine. In case of doubt, it is usually safe to
choose the default option, or get your best guess and see what happens!
Remember: you can always shutdown the machine and restart the installation, if
needed. The goal is for you to find your own way through the installer. However,
you can still research online, or ask us if more information is needed.

In each step, you should try to make progress only with the short instruction
provided upfront. If this is the first time you go through this process, it is
possible that you need a more in depth explanation, you can ask us to
disclose it (one at a time, only if needed). If that is not enough, you can then
ask for hints on how to proceed. As a last resource, you can ask observers to
provide a step-by-step and unblock the process.

**We ask you that you describe what you're doing as you're doing it to help us understand your train of thoughts**

## Part A - Prepare the bootable medias with Debian 12

1. Head to the Debian project website (debian.org) and download the Debian 12 image

2. On the same website, look for an alternative image to try Debian before installing ("live" image). Choose the option with the Gnome desktop environment. 

3. Flash both iso images onto the provided USB keys.

4. Proceed to the installation. You'll boot from both USB keys, one at a time, and later compare your experience. The observer will tell you which one to perform first, 1B or 2B. Both processes should deliver a fully installed system in the laptop.

## Part 1B -  Debian Installer (netinst image)

1. Once the installer menu shows up, select "Graphical install"

2. Select the basics: language, location and keyboard

3. Configure the network: select the wireless interface, then choose
the network with the provided password

4. Choose hostname and domain name
   
5. Set up users and passwords
   
6. Configure the clock

7. Partition your disk: erase and reuse the entire disk

8. Configure the package manager. No need to scan extra installation media. Choose a network mirror.

9. Option to participate in popularity contest

10. Select and install software

11. Accept to install the boot loader on the primary disk (/dev/sda)

12. Reboot, remove the USB, and explore your new system

13. Login using the user you created during installation

14. Try to change your desktop wallpaper, open a few windows, have a taste of the system!

15. Note down 1-3 words to describe how do you feel about this process (1B).
   
## Part 2B - Calamares Installer (live image)

1. Once the boot menu shows up, select the "Live system" option.

2. Once you boot, you'll be greeted by Gnome's initial setup. You can safely choose the default options. Then configure the WiFi network with the provided password.

3. Click around and explore the system. Try to change the wallpaper, open a browser, have a taste of the system!

4. Click on the icon "Install Debian", which will open the Calamares installer for Debian 12.

5. Select the basics: language, location and keyboard.
   
6. Partitions: choose "Erase disk".

7. Configure your user.

8. Install.

9. Restart your system, remove the USB, and explore your newly installed Debian.

10. Login using the user you created during installation.

11. Note down 1-3 words to describe how do you feel about this process.

## Part C - Evaluate your experience

1. Fill out the Feedback form

 **Well done, you've completed all the tasks!**
