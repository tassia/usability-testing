You can choose your favorite name for a hostname, because you'll see that on a
daily basis, and leave the domain name empty, which makes no difference.
