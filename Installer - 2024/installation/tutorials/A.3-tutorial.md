Download BalenaEtcher from https://etcher.balena.io/. If you don't have admin priviledges in the computer at hands, use the portable version of BalenaEtcher.

Then, for each of those image files:
- "Flash from file", chose an image file (.iso)
- "Select target", chose the usb key
- click on "flash" and wait!
