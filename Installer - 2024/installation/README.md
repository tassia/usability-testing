| **Part A**  | **verbose**   | **hint** | **tutorial** |
| ---   |---            |---|---|
| A.1   | [A.1-verbose](verbose-tasks/A.1-verbose.md)  |  [A.1-hint](hints/A.1-hint.md) | [A.1-tutorial](tutorials/A.1-tutorial.md)  |
| A.2   | [A.2-verbose](verbose-tasks/A.2-verbose.md)  |  [A.2-hint](hints/A.2-hint.md) | [A.2-tutorial](tutorials/A.2-tutorial.md)  |
| A.3   | [A.3-verbose](verbose-tasks/A.3-verbose.md)  |  [A.3-hint](hints/A.3-hint.md) | [A.3-tutorial](tutorials/A.3-tutorial.md)  |
| **Part 1B**  | **verbose**   | **hint** | **tutorial** |
| 1B.1   | [1B.1-verbose](verbose-tasks/1B.1-verbose.md)  |  [1B.1-hint](hints/1B.1-hint.md) | [1B.1-tutorial](tutorials/1B.1-tutorial.md)  |
| 1B.2   | [1B.2-verbose](verbose-tasks/1B.2-verbose.md)  |  [1B.2-hint](hints/1B.2-hint.md) | [1B.2-tutorial](tutorials/1B.2-tutorial.md)  |
| 1B.3   | [1B.3-verbose](verbose-tasks/1B.3-verbose.md)  |  [1B.3-hint](hints/1B.3-hint.md) | no tutorial  |
| 1B.4   | [1B.4-verbose](verbose-tasks/1B.4-verbose.md)  |  [1B.4-hint](hints/1B.4-hint.md) | [1B.4-tutorial](tutorials/1B.4-tutorial.md)  |
| 1B.5   | [1B.5-verbose](verbose-tasks/1B.5-verbose.md)  |  [1B.5-hint](hints/1B.5-hint.md) | no tutorial  |
| 1B.6   | [1B.6-verbose](verbose-tasks/1B.6-verbose.md)  |  [1B.6-hint](hints/1B.6-hint.md) | [1B.6-tutorial](tutorials/1B.6-tutorial.md)  |
| 1B.7   | [1B.7-verbose](verbose-tasks/1B.7-verbose.md)  |  [1B.7-hint](hints/1B.7-hint.md) | [1B.7-tutorial](tutorials/1B.7-tutorial.md)  |
| 1B.8   | [1B.8-verbose](verbose-tasks/1B.8-verbose.md)  |  [1B.8-hint](hints/1B.8-hint.md) | [1B.8-tutorial](tutorials/1B.8-tutorial.md)  |
| 1B.9   | [1B.9-verbose](verbose-tasks/1B.9-verbose.md)  |  [1B.9-hint](hints/1B.9-hint.md) | no tutorial  |
| 1B.10  |[1B.10-verbose](verbose-tasks/1B.10-verbose.md)  |[1B.10-hint](hints/1B.10-hint.md) | no tutorial  |
| **Part 2B**  | **verbose**   | **hint** | **tutorial** |
| 2B.4   | [2B.4-verbose](verbose-tasks/1B.2-verbose.md)  |  [2B.4-hint](hints/1B.2-hint.md) | [2B.4-tutorial](tutorials/1B.2-tutorial.md)  |
| 2B.5   | [2B.5-verbose](verbose-tasks/1B.7-verbose.md)  |  [2B.5-hint](hints/1B.7-hint.md) | [2B.5-tutorial](tutorials/1B.7-tutorial.md)  |
| 2B.6  |[2B.6-verbose](verbose-tasks/1B.5-verbose.md)  |[2B.6-hint](hints/1B.5-hint.md) | no tutorial  |

