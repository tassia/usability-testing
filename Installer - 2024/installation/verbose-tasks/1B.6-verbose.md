### Configure the clock

Based on the location previously given, the installer will offer you a list of
available timezones for that particular country.

Hint: [1B.6-hint](../hints/1B.6-hint.md)
