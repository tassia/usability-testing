### Head to the Debian project website and download the Debian 12 image

You are looking for an .iso file containing the Debian 12 installer image.

Hint:  [A.1-hint](../hints/A.1-hint.md)
