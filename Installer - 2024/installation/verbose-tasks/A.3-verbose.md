### Flash the iso image onto the provided USB keys

This step is needed so that we are able to boot into those images using USB keys and start the installation process. Search online for how you can flash USB keys for clues if you don't know how to!

Hint: [A.3-hint](../hints/A.3-hint.md)