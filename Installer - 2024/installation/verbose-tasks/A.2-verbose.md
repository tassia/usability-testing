### Head to the Debian project website and download the Debian live image

Apart from the regular instaler (netinst), Debian provides several other options, such as a smaller installer, installer for other architectures, and also those "live" images, which allow you to run the system from the RAM memory, without installing on disk. Once you boot from a live image, you'll have the option to install the running system on disk.

There are several live images with different Desktop environments available. We will use the Gnome option, which is the same installed by default with the regular installer. 

Hint:  [A.2-hint](../hints/A.2-hint.md)