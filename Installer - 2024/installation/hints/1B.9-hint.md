It is a very personal choice. If you want to help, choose "Yes"; but if you have
any concerns about sending data, even if anonymized, you might consider saying
"No".
