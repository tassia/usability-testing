On Windows CMD, you could use the command *certutil* for this task. On Linux bash,
*sha512sum* does the job.

Tutorial: [1.2-tutorial](/installation/tutorials/1.2-tutorial.md) 
