# Verbose Tasks for Installation of Debian 12 (Yellow)

## Before you start

There might be many concepts you might not fully understand in the installation process, and that is totally fine. Take notes, research online, or ask observers if needed. If you don't understand something, feel free to choose the default option and it should be fine. 

Remember: you can always shutdown the machine and restart the installation if needed. The goal is for you to find your own way through the installer. However, you can still research online or ask us if more information is needed.

**We ask you that you describe what you're doing as you're doing it to help us understand your train of thoughts**

## Getting the Debian 12 image onto a USB key

Before starting the installation process, we recommend taking a quick look at the [documentation](https://www.debian.org/doc/) for any guides and/or help you might need.

1. Head to the [Debian](https://www.debian.org/) page to download the Debian 12 image (an .iso file).

2. Once the .iso file is downloaded, proceed with the instructions on the [page](https://www.debian.org/download) to verify your download with the checksum. <br>
    A checksum is a value that proves the integrity of a file, or that proves the file has not been tampered with. By verifying the checksum of your downloaded file with the checksum provided by Debian, we are able to make sure that these two match and that the file is not corrupted.

3. Verify that the checksums file provided was signed by the Debian CD signing key.<br>
   These images that have been released by Debian are not only accompanied by a checksum but also by signatures. To make sure that the checksums files themselves are correct, we verify that the key used for the signature really belongs to Debian (ex. in the debian.org domain).

4. Flash the iso image onto the provided USB key<br>
    This step is needed so that we are able to boot into the image through the usb key and start the installation process.

5. Boot into the USB key we just flashed.<br>
    To boot into something means to load an operating system into the computer's main memory or RAM. Once the operating system is loaded (for example, on a Windows PC), we will see the initial Windows screen.

## Starting the Installation 

1. Choose the installation type. <br>
   A "graphical install" refers to the installer having a graphical interface, which means the user will be able to interact using buttons, menus, icons and text fields as opposed to a command line. 

2. Once the installation starts, choose your language of preference. <br>
   This is the language of both the installation process and the operating system after it's been installed.

3. Select your location.<br>
   This will usually be the country where you currently live in. It uses the selected location to set the appropriate time zone.

4. Select the keyboard layout that matches your physical keyboard.<br>
   For example, French keyboards contain the letter **é** in the place of **/** for american keyboards and so the layouts for these keyboards are also different.

5. Configure your network. <br>
   If your laptop has multiple interfaces (ethernet, wifi), the installer will list them and give you the option to choose. In our case, we will be using WiFi.

6. Set a root password. <br>
   This will set a password for the *root* user account, which is the system administrative account. This specific account has access to everything on the system.

7. Create a new user account. <br>
   This user account is a regular user on the system. Unlike the root user we previously configured, this user does not have administrative permissions (access to all). It is considered a good security practice to only use a root user for system-related tasks.

8. Partition your disk. <br>
   Partitioning a disk simply refers to creating different "regions" on a disk. This will allow you to choose how much storage you want for each partition and each part of the system.

9. Configure the package manager. <br>
   You will be prompted to scan extra installation media. This is used to identify any supplementary media that can be utilized by the package manager. 

10. Choose the network mirror. <br>
    Configuring this ensures that the latest packages are installed using the internet, even though a lot of these is included in the installation media (the iso).
 
11.  Option to participate in popularity contest configuration. <br>
     By chooseing "Yes", you consent to sending package statistics to the developers on a weekly basis. This information helps developers in improving software development.

12. Select your software.
    Only the core components of the system have been installed. To customize the system, you can choose to install one or more predefined collections of software. This is optional.

13. Option to install GRUB.
    GRUB is a boot loader like, for example, Windows Boot Manager. This will be in charge of loading the operating system onto the computer. If you have another operating system already installed, the other system might be temporarily unavailable.

After this, the installation will be complete and you'll need to reboot the system and remove the USB key.

## After the Installation

1. Login using the user you created.
   You can use either the root user, or the user created during installation.

2. Change your wallpaper
   This will change the wallpaper of your Desktop. Think of how you would usually do this on your own computer. Play around until you figure it out!


**Well done, you've completed all the tasks!**
