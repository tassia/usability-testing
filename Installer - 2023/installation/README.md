| **Part 1**  | **verbose**   | **hint** | **tutorial** |
| ---   |---            |---|---|
| 1.1   | [1.1-verbose](verbose-tasks/1.1-verbose.md)  |  [1.1-hint](hints/1.1-hint.md) | [1.1-tutorial](tutorials/1.1-tutorial.md)  |
| 1.2   | [1.2-verbose](verbose-tasks/1.2-verbose.md)  |  [1.2-hint](hints/1.2-hint.md) | [1.2-tutorial](tutorials/1.2-tutorial.md)  |
| 1.3   | [1.3-verbose](verbose-tasks/1.3-verbose.md)  |  [1.3-hint](hints/1.3-hint.md) | [1.3-tutorial](tutorials/1.3-tutorial.md)  |
| 1.4   | [1.4-verbose](verbose-tasks/1.4-verbose.md)  |  [1.4-hint](hints/1.4-hint.md) | [1.4-tutorial](tutorials/1.4-tutorial.md)  |
| 1.5   | [1.5-verbose](verbose-tasks/1.5-verbose.md)  |  [1.5-hint](hints/1.5-hint.md) | [1.5-tutorial](tutorials/1.5-tutorial.md)  |
| **Part 2**  | **verbose**   | **hint** | **tutorial** |
| 2.1   | [2.1-verbose](verbose-tasks/2.1-verbose.md)  |  [2.1-hint](hints/2.1-hint.md) | [2.1-tutorial](tutorials/1.1-tutorial.md)  |
| 2.2   | [2.2-verbose](verbose-tasks/2.2-verbose.md)  |  [2.2-hint](hints/2.2-hint.md) | [2.2-tutorial](tutorials/1.2-tutorial.md)  |
| 2.3   | [2.3-verbose](verbose-tasks/2.3-verbose.md)  |  [2.3-hint](hints/2.3-hint.md) | no tutorial  |
| 2.4   | [2.4-verbose](verbose-tasks/2.4-verbose.md)  |  [2.4-hint](hints/2.4-hint.md) | [2.4-tutorial](tutorials/2.4-tutorial.md)  |
| 2.5   | [2.5-verbose](verbose-tasks/2.5-verbose.md)  |  [2.5-hint](hints/2.5-hint.md) | no tutorial  |
| 2.6   | [2.6-verbose](verbose-tasks/2.6-verbose.md)  |  [2.6-hint](hints/2.6-hint.md) | [2.6-tutorial](tutorials/2.6-tutorial.md)  |
| 2.7   | [2.7-verbose](verbose-tasks/2.7-verbose.md)  |  [2.7-hint](hints/2.7-hint.md) | [2.7-tutorial](tutorials/2.7-tutorial.md)  |
| 2.8   | [2.8-verbose](verbose-tasks/2.8-verbose.md)  |  [2.8-hint](hints/2.8-hint.md) | [2.8-tutorial](tutorials/2.8-tutorial.md)  |
| 2.9   | [2.9-verbose](verbose-tasks/2.9-verbose.md)  |  [2.9-hint](hints/2.9-hint.md) | no tutorial  |
| 2.10  |[2.10-verbose](verbose-tasks/2.10-verbose.md)  |[2.10-hint](hints/2.10-hint.md) | no tutorial  |
| **Part 3**  | **verbose**   | **hint** | **tutorial** |
| 3.1   | [3.1-verbose](verbose-tasks/3.1-verbose.md)  |  no hint | no tutorial  |
| 3.2   | [3.2-verbose](verbose-tasks/3.2-verbose.md)  |  no hint | no tutorial  |
| 3.3   | [3.3-verbose](verbose-tasks/3.3-verbose.md)  |  [3.3-hint](hints/3.3-hint.md) | [3.3-tutorial](tutorials/3.3-tutorial.md)  |

