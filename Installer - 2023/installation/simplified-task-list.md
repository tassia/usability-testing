# Simplified Tasks for Installation of Debian 12 (Green)

## Before you start

There might be many concepts you do not fully understand in the installation
process, and that is totally fine. In case of doubt, it is usually safe to
choose the default option, or get your best guess and see what happens!
Remember: you can always shutdown the machine and restart the installation, if
needed. The goal is for you to find your own way through the installer. However,
you can still research online, or ask us if more information is needed.

In each step, you should try to make progress only with the short instruction
provided upfront. If this is the first time you go through this process, it is
possible that you need a more in depth explanation, you can ask us to
disclose it (one at a time, only if needed). If that is not enough, you can then
ask for hints on how to proceed. As a last resource, you can ask observers to
provide a step-by-step and unblock the process.

PS: Part 1 of this study is the hardest, don't be discouraged, it gets better :)

**We ask you that you describe what you're doing as you're doing it to help us understand your train of thoughts**

## Part 1 - Prepare a bootable media with Debian 12

Before starting the installation process, you could take a quick look at debian.org/doc for the
[documentation](https://www.debian.org/doc/) for any guides and/or help you
might need later.

1. Head to the Debian project website (debian.org) and download the Debian 12 image

2. Follow instructions on the site (debian.org/download) and verify with the checksum provided that your image file (.iso) is authentic

3. Verify that the checksum provided is authentic by checking that it was signed by the Debian CD signing key

4. Flash the iso image onto the provided USB key

5. Boot from the USB key you have just flashed

## Part 2 - Proceed to the installation 

1. Once the installer menu shows up, select the installation type

2. Select the basics: language, location and keyboard

3. Configure the network: select the wireless interface, then choose
the network with the provided password

4. Choose hostname and domain name
   
5. Set up users and passwords
   
6. Configure the clock

7. Partition your disk: erase and reuse the entire disk

8. Configure the package manager: choose a network mirror

9. Option to participate in popularity contest

10. Select and install software

## Part 3 - Reboot and explore your new system

1. Boot on the newly installed system

2. Login using the user you created during installation

3. Change your desktop wallpaper

4. Open a few windows and take a screenshot

5. Send an email to ga@bouzon.com.br: attach the screenshot, and answer the
following question:

> In general, how smooth was this process for you? Are there particular
> sections that you find confusing? What would you ask to the Debian
> installer developers if you had a chance? (this is your chance, indeed!)

**Well done, you've completed all the tasks!**
