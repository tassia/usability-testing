### On Windows

Head to https://gnupg.org/download/, scroll down to "binary releases" and look
for "GPG4Win". Click the big download button. It should take you to a Paypal page, you can just choose to donate $0 and continue to the download.

Set up GnuPG and open Kleopatra 

Kleopatra:

On the top, you should see a button for "Lookup on Server" 
It should open a search page. Type in the Debian email **debian-cd@lists.debian.org** and click on Search.
After a while, it should show diffferent keys. Choose all that have the email above and click on "Import"
After loading, all the keys should show under "All certificates" in Kleopatra.


To make sure that the imported keys from the step above are trusted (so that Kleopatra understand that these keys are correct), you need to create your own pair of keys and certify the debian keys.

At the top left corner, go on "File", "New OpenPGP Key Pair". After that, right click on
each key from debian-cd and "Certify...".

The last step is to verify that the provided signature (https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA512SUMS.sign) comes
from their checksums file (https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA512SUMS)

Download the 2 files from the webpage,
then click on "Decrypt/Verify", choose the signature file that you downloaded (SHA512SUMS.sign), and then the
checksums file (SHA512SUMS). If all went well, you should get "Valid signature by
debia-cd@lists.debian.org".


### On Linux Bash and/or Windows (follow GnuPG installation and setup)

> gpg --keyserver keyring.debian.org  --recv-keys  0x988021A964E6EA7D \
> gpg --keyserver keyring.debian.org  --recv-keys  0xDA87E80D6294BE9B \
> gpg --keyserver keyring.debian.org  --recv-keys  0x42468F4009EA8AC3 

> wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA512SUMS -o SHA512SUMS \
> wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA512SUMS.sign -o SHA512SUMS.sign

> gpg --verify SHA512SUMS.sign SHA512SUMS

This should return a message containing the email (debian-cd@lists.debian.org) if all goes well and the signing key which should match one of the keys listed in 
https://www.debian.org/CD/verify (check for the key fingerprint)
