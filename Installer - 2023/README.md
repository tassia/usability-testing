# Usability testing in Debian: Installer - 2023

We use this repo to collaborate on the planning, execution and result
consolidation of usability tests in Debian. This folder contains documents
from the first phase of the project, that took place in 2023.

## The Debian installation process

This round of tests aimed to tackle the Debian installation process.
Not only the installer per se, but the whole process of finding the iso,
verifying it, and finally installing the system.

We investigated how easily users could find their way into the
installation process, trying to uncover issues that might prevent users
from using Debian.

Instructions to [testers](instructions/instruction-testers.md) and
[observers](instruction-observers.md) provide more details about the setup and
unfolding of tests.  

## Timeline

* Pilot run: August 22nd, 2023
* First phase: August 24th - September 1st, 2023

## Acknowledgement

The main computer used for testing installations was funded by the Debian
Project. Thanks to all donors for [supporting Debian](https://www.debian.org/donations).

## Authors

This study was conducted by Tassia Camoes Araujo, Anthony Nadeau and
Giuliana Bouzon.

## Contributing

Comments, suggestions and merge requests are more than welcome! You can reach us
through email or through [Discord](https://discord.gg/rBKb9JRQGD).

We are also open to organizing usability tests in other areas of Debian or FLOSS
in general.