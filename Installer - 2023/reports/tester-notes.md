# Tester Notes - Usability Tests

## Tester 1 - August 24th, 2023 - Windows 11

### Time

From 10:16 AM to 12:11 AM

Took 1h55m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 6min - 4 <br>
   The tester selected the download button from debian.org, but searched for a while afterwards to find the actual file to download.

2. Checksum verification: 23min - 2 <br>
   The tester clicked on the checksum link on the download page for the iso, but because there was no documentation to clarify *how* to verify the checksum, they assumed the step was complete. When told that there was more to be done, they viewed the verbose task to gain more information. With the verbose, they understood what the checksum was but needed more information on how to proceed so they viewed the hint. The hint allowed them to research the certutil command and from research they found on Google, they were able to verify the checksum using that command.

3. Signature verification: 36min - 1 <br>
   The tester started by researching the task on Google. They then assumed that it would be a similar process to the checksum verification and tried to use the certutil command again using various hashing algorithms. They researched both PGP and GPG to understand what they meant. From there they could not find any helpful resources and referred to the verbose, then the hint, and finally the tutorial to get a better idea of what to do. Using the tutorial they were able to successfully complete the task.
   
4. Flashing the usb key: 11min - 2 <br>
   The tester did not understand the task and directly used the verbose. From there, they researched software that could flash an iso to a USB and started by downloading the free bootable media creator by EaseUS. Once finding that the software only supports Windows, the tester referred to the hint and then was able to install balenaEtcher and successfully flash the iso onto the USB key.

5. Booting from the usb key: 9min - 1 <br>
   The tester did not know how to boot into a USB and used the verbose immediately. They then tried various keys on the keyboard to try to interrupt the boot sequence. After a few attempts, they looked at the hint and the tutorial to boot into the USB key.

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 3 <br>
   They needed to look at verbose as they were not sure what the different options meant. Then picked *Graphical Install*.

2. Language, location and keyboard: 2min - 4 

3. Network configuration: 1min - 3 <br>
   The tester used verbose to find out which interface to select. They then tried setting up with WEP. When the DHCP connection failed they went back and selected WPA/WPA2.

4. Hostname and domain name: 1min - 4 <br>
   The tester created a random hostname and domain name and proceeded.
   
5. Users configuration: 1min - 4
   
6. Clock configuration: 1min - 4 

7. Disk partitioning: 4min - 4 <br>
   The tester selected *Guided - use entire disk*, and repeatedly pressed continue to get through this task.

8. Network mirror: 2min - 4

9. Popularity Contest: 1min - 4 ("Yes")

10. Software Installation: 10min - 4 (installation took long)

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: <1min - 4

3. Wallpaper change: 1min - 4

Tester Comment: The tester had difficulty with part 1 because there were many unfamiliar terms. They suggested the addition of some documentation to explain how to verify a checksum and digital signature directly on the download page. They also suggested that having a list of software that supports flashing linux iso files to USB keys would be helpful.


## Tester 2 - August 24th, 2023 - Windows 11

### Time

From 1:28 PM to 2:35 PM

Took 1h07m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 26min - 4 (Downloading the iso took a significantly long time due to a weak connection)
   
2. Checksum verification: 5min - 1 <br>
   The tester was unfamiliar with the term "checksum" and researched it. After a bit of research, they referred to the verbose, followed immediately by the hint, and then the tutorial. Afterwards, they required a bit of assistance from the observer to understand the tutorial and to successfully verify the checksum.

3. Signature verification: 4min - 1 <br>
   The tester was unfamiliar with digital signatures, and so they referred directly to the verbose, hint, and tutorial. The tutorial was sufficient in guiding the tester to the verification of the signature.
   
4. Flashing the usb key: 9min - 2 <br>
   The tester initially tried to copy+paste the iso onto the USB key. When told that the task was not technically complete, the tester referred to the verbose and then the hint, and was then able to download Balena Etcher and successfully flashed the iso image onto the USB key.

5. Booting from the usb key: 1min - 2 <br>
   The tester did not know how to boot into a USB and used the verbose and the hint immediately. Using the hint, they accessed the boot menu and successfully booted into the USB key.

### Part 2 - Proceed to the installation 

1. Installation type: 3min - 3 <br>
   They needed to look at verbose as they were not sure what the different options meant. Then picked *Graphical Install*.

2. Language, location and keyboard: <1min - 4

3. Network configuration: 2min - 4 <br>
   The tester tried setting up with WEP. When the DHCP connection failed they went back and selected WPA/WPA2.

4. Hostname and domain name: <1min - 4 <br>
   The tester created a random hostname and domain name and proceeded.
   
5. Users configuration: <1min - 4
   
6. Clock configuration: 2min - 4 

7. Disk partitioning: 6min - 4 <br>
   The tester selected *Guided - use entire disk*, and repeatedly pressed continue to get through this task.

8. Network mirror: 2min - 4 <br>
   The tester asked what to put for HTTP proxy, but managed to read the installer page and proceed without intervention or help from the observer.

9. Popularity Contest: 1min - 4 ("No")

10. Software Installation: 10min - 4 (installation took long)

### Part 3 - Reboot and explore your new system

1. Booting into the system: <1min - 4

2. Login: <1min - 4

3. Wallpaper change: 1min - 4

Tester Comment: The tester said the process was generally easy given the documentation we prepared, but some concepts were difficult to understand. They also noted that in the installer, they were not familiar with many of the things it was asking and so they clicked through with a sense of hoping that things wouldn't break.


## Tester 3 - August 25th, 2023 - Windows 11

### Time

From 10:17 AM to 11:51 AM

Took 1h35m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 2min - 4

2. Checksum verification: 15min - 2 <br>
   The tester looked through the Debian documentation, found it confusing and tried to skip the step. They looked at both the verbose and the hint and with that information, they were able to use Google to find online tutorials on how to accomplish the task. They were also confused about which hash algorithm was used to calculate the checksum since it was not clear from the documentation.

3. Signature verification: 16min - 1 <br>
   The tester was very confused and had no idea what to do; they felt like there wasn't even enough information to be able to do a Google search. They looked at both the verbose and the hint and tried to figure it out on their own but needed the tutorial to accomplish the task.
   
4. Flashing the usb key: 7min - 2 <br>
   The tester wasn't sure what this task entailed and looked at the verbose. They used the information from the verbose to try and found an online tutorial but that didn't help much so they looked at the hint.

5. Booting from the usb key: 9min - 2 <br>
   The tester was not sure what this meant either and looked at the verbose and then the hint. They were very confused about this whole process.

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 3 <br>
   They needed to look at verbose as they were not sure what the different options meant. Then picked *Graphical Install*.

2. Language, location and keyboard: 1min - 4

3. Network configuration: 1min - 4 <br>
    They were a bit confused about which interface to choose as they did not know what each one meant.

4. Hostname and domain name: 4min - 2 <br>
   They chose the default option for hostname but when it came to the domain name, they thought they needed the right Debian domain for it to work and so they looked at both the verbose and the hint for this.
   
5. Users configuration: 2min - 4
   
6. Clock configuration: 1min - 4 

7. Disk partitioning: 3min - 3 <br>
   They chose *Guided - use entire disk* but they were not sure what the difference was between **guided** and **manual** and simply chose the first option. Later on, they were also confused about which disk to choose and chose the USB key instead of the computer disk at first.

8. Network mirror: 1min - 4 <br>
   Didn't know what this meant so just chose default and left HTTP proxy blank.

9. Popularity Contest: 1min - 4 ("No") <br>
   Felt uncomfortable choosing "Yes" because don't want to share data.

10. Software Installation: 10min - 4 (installation took long)

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: 1min - 4

3. Wallpaper change: 2min - 4 <br>
   Because there was no apparent taskbar and the application button is very small and at the top, took them a second to figure out what to do.


## Tester 4 - August 25th, 2023 - Windows 11

### Time

From 12:10 PM to 2:16 PM

Took 2h6m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 4min - 2 <br>
   They Googled "Debian 12 image" and went to Debian documentation and used CTRL + F and could not find anything relating to the download of an image file. Looked at the verbose and at the hint. They finally found the download button but they were confused as the file was not downloading - didn't realize they then had to click the netinst button.

2. Checksum verification: 18min - 2 <br>
   They looked at both the verbose and the hint, then googled tutorials on how to get the checksum but did not realize there were different algorithms so the checksum was not matching. They also needed extra verbal explanation on how exactly to verify a checksum after retrieving it. They at first just looked at the checksum on the Debian website and thought that was all.

3. Signature verification: 21min - 1 <br>
   The tester was very confused and had no idea what to do; they had all pieces of information needed (signature, key and checksum file) but did not know what to do with them. They needed to look at the verbose, hint and tutorial to accomplish the task. 
   
4. Flashing the usb key: 10min - 2 <br>
   The tester tried to copy paste the file so I needed to explain that it's not the same as flashing or burning it. Then they needed to look at the verbose for an explanation and using that, they were able to Google and find their way through.

5. Booting from the usb key: 14min - 3 <br>
   The tester Googled it, then looked at verbose and edited the boot order for this (did not use F12 for boot menu)

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 4 <br>
   They were not sure what the different options meant so they choose *Install*.

2. Language, location and keyboard: 1min - 4

3. Network configuration: 3min - 3 <br>
    They did not understand the difference betweeen *WEP* and *WPA/WPA2* when asked to choose type of WiFi so chose WEP at first and when it didn't work, chose the other one. 

4. Hostname and domain name: 4min - 3 <br>
   They did not know what this was so they looked at the verbose and then picked random for both options. Didn't know what this was for or if they needed it.
   
5. Users configuration: 2min - 4
   
6. Clock configuration: 1min - 4 

7. Disk partitioning: 3min - 3 <br>
   They chose *Guided - use entire disk* but they were not sure what the difference was between **guided** and **manual** and simply chose the first option. Later on, they were also confused about which disk to choose and chose the USB key instead of the computer disk at first.

8. Network mirror: 1min - 3 <br>
   Didn't know what this meant so just chose default. Entered something for HTTP proxy (made something up) but it didn't work (http://[name:hello123]host[:port]/). They needed to look at verbose to understand they could leave it blank but overall did not know what this was for.

9. Popularity Contest: 1min - 4 ("No") <br>
   Felt uncomfortable choosing "Yes" because don't want to share data.

10. Software Installation: 10min - 4 (installation took long) <br>
    Chose default options because they did not know the difference between the options.

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: 1min - 4

3. Wallpaper change: 2min - 4 <br>
   Right-click and then "Change Wallpaper" button. Seemed very intuitive.


## Tester 5 - August 25th, 2023 - Windows 11

### Time

From 3:12 PM to 4:45 PM

Took 1h33m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 1min - 4

2. Checksum verification: 8min - 3 <br>
   They looked at both the verbose, and googled a tutorial to get a checksum using CMD. They did not know there were different algorithms for checksums and were confused when it didn't match and did not know where to find the algorithm for the checksum provided by Debian. Took some looking around.

3. Signature verification: 33min - 1 <br>
   Tried doing a google search and reading the Debian documentation but were confused about what exactly they needed to do (didn't understand what a signing key or a signature was for). Even thought they tried to do google searches, because it was unclear what they needed to do, it was also unclear what to search for so they ended up needing the tutorial at the end.
   
4. Flashing the usb key: 5min - 4 <br>
   Googled how to flash a usb key and found an article about Rufus. It was fairly simple to use it thereafter.

5. Booting from the usb key: 3min - 4 <br>
   They knew what to do, went to BIOS after startup, edited the boot orderand let it reboot.

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 4 <br>
   Understood that a *graphical* install vs an install meant the use of graphical components, aka a GUI.

2. Language, location and keyboard: 1min - 4

3. Network configuration: 2min - 4 <br>
   Understood that, because the WiFi had a password, we were using WPA.

4. Hostname and domain name: 2min - 4 <br>
   Didn't know what this was for or if they needed it so randomly entered something for hostname and left domain name blank.
   
5. Users configuration: 2min - 4
   
6. Clock configuration: 1min - 4 

7. Disk partitioning: 3min - 4 <br>
   They chose *Guided - use entire disk*. Chose default options for everything as they were not sure what each one meant.

8. Network mirror: 1min - 3 <br>
   Didn't know what this meant so just chose default. Left HTTP Proxy blank.

9.  Popularity Contest: 1min - 4 ("No") <br>
    Felt uncomfortable choosing "Yes" because don't want to share data.

10. Software Installation: 10min - 4 (installation took long) <br>
    Chose default options because they did not know the difference between the options.

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: 1min - 4

3. Wallpaper change: 2min - 4 <br>
   Right-click and then "Change Wallpaper" button. Seemed very intuitive.


## Tester 6 - August 25th, 2023 - Debian

### Time

From 5:10 PM to 7:27 PM

Took 2h17m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 2min - 4

2. Checksum verification: 24min - 2 <br>
   They looked at the verbose for more explanation then went through the Debian documentation looking for how to do it. Then they found a tutorial online and were able to complete this task. This took long because they tried to find the answer only through Debian documentation at first but couldn't find anything and thought it was confusing.

3. Signature verification: 45min - 1 <br>
   Found all the pieces but did not understand what to do with them or how the whole process worked. Looked through many different links on website documentation like the debian keyring, cd verify, installation guide, etc. Then, they googled how to verify signatures and found a tutorial with *gpg* commands and through lots of trial and error and googling, they were able to make it work but not fully (missed the step to certify the keys added by creating their own key pair). Also went through many different tutorials online before they found one that worked.
   
4. Flashing the usb key: 21min - 3 <br>
   Looked at Debian documentation on USB keys but didn't find anything that could help them. Then they googled how to flash a USB key and came across an article with commands using *fdisk* and *dd* but there were problems finding the pieces needed for the command (like the device). Overall, a lot of research was needed.

5. Booting from the usb key: 5min - 4 <br>
   Googled how to boot from usb key for Lenovo laptops and then used F12. 

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 4 <br>
   Understood that a *graphical* install vs an install meant the use of graphical components, aka a GUI.

2. Language, location and keyboard: 1min - 4

3. Network configuration: 4min - 4 <br>
   Chose default option (WPA) when asked to choose between WEP and WPA because did not know the difference.

4. Hostname and domain name: 2min - 4 <br>
   Didn't know what this was for or if they needed it so randomly entered something for domain name and left host name blank.
   
5. Users configuration: 2min - 4
   
6. Clock configuration: 2min - 4 <br>
   Had to google location's timezone as they were not sure what their timezone was.

7. Disk partitioning: 3min - 4 <br>
   They chose *Guided - use entire disk*. Chose default options for everything as they were not sure what each one meant.

8. Network mirror: 1min - 3 <br>
   Didn't know what this meant so just chose default. Left HTTP Proxy blank.

9.  Popularity Contest: 1min - 4 ("No") <br>
    Felt uncomfortable choosing "Yes" because don't want to share data.

10. Software Installation: 10min - 4 (installation took long) <br>
    Chose default options because they did not know the difference between the options, then chose web server and SSH server because recognized the words.

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: 1min - 4

3. Wallpaper change: 3min - 4 <br>
   Went on Gnome Help app for help but didn't find anything so they played around and finally found the background settings in "Settings"


## Tester 7 - August 28th, 2023 - Windows 11

### Time

From 10:24 AM to 11:32 AM

Took 1h08m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 1min - 4
   
2. Checksum verification: 10min - 1 <br>
   The tester was unfamiliar with the term "checksum" and researched it. After a bit of research and exploring the Debian documentation, they referred to the verbose. From there, they clicked the checksum on the download page, and then opened the hint. The tester opened the CMD and tried running certutil with no parameters. They checked the tutorial and ran the proper command and successfully verified the checksum.

3. Signature verification: 13min - 1 <br>
   The tester was unfamiliar with digital signatures. They clicked on the signature on the download page and read some documentation linked on the download page. From there they used the verbose and then the hint. The tester researched GnuPG and downloaded gpg4win. Then they used the tutorial, opened Kleopatra, downloaded the checksum and signature through the browser, and successfully verified the signature in Kleopatra.
   
4. Flashing the usb key: 10min - 2 <br>
   The tester initially tried to copy+paste the iso onto the USB key. When told that the task was not technically complete, the tester researched what flashing a USB was and how to do it. They found and downloaded the EaseUS media creation tool, but was blocked when it said it was only for windows. Then they went back to their research page, and found the link for rufus. They downloaded rufus and successfully flashed the USB key.

5. Booting from the usb key: 6min - 2 <br>
   The tester was able to access the startup menu, but from there was having difficulties booting into the USB key. They used the verbose and then the hint and explored some sub-menus. Then they used the tutorial, changed the boot priority order and successfully booted into the USB key.

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 4  <br>
   The tester selected the *Graphical Install* option.

2. Language, location and keyboard: 1min - 4

3. Network configuration: 1min - 4

4. Hostname and domain name: <1min - 4 <br>
   The tester created a random hostname and domain name and proceeded.
   
5. Users configuration: 2min - 4
   
6. Clock configuration: <1min - 4 

7. Disk partitioning: 3min - 4 <br>
   The tester selected *Guided - use entire disk*, and repeatedly pressed continue to get through this task.

8. Network mirror: 3min - 4

9. Popularity Contest: 2min - 4 ("Yes")

10. Software Installation: 10min - 4 (installation took long)

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: <1min - 4

3. Wallpaper change: 1min - 4

Tester Comment: The tester noted that overall, they found that the installation process more difficult than other operating systems that they have used. They described the initial checksum and signature verification to be "impossible" without the provided tutorial. However, they noted that after flashing the USB key and booting into it, the installation process felt similar to other operating system installations.


## Tester 8 - August 28th, 2023 - Windows 11

### Time

From 12:06 PM to 1:27 PM

Took 1h21m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 4min - 4
   
2. Checksum verification: 11min - 3 <br>
   The tester started by trying to find the checksum by opening the iso as a folder. They read through Debian documentation and searched the task on Google. Then they opened the command prompt and used the certutil command as shown in a website they found, but they put the wrong hashing algorithm. They went back to the Debian documentation, and asked the observer if the task was complete. When told what output to expect from certutil, they changed the command to the correct hashing algorithm and successfully verified the checksum.

3. Signature verification: 26min - 1 <br>
   The tester believed that the signature and checksum were the same thing and almost skipped the task. They then found GPG package documentation on the Debian website, and they researched the process to verify a signature. They tried the gpg command in the command prompt, and when it failed, they tried in powershell. When the command failed again, they referred to the verbose and the hint and then went to the GPG website. They downloaded the wrong file, but then found a series of other websites where they could download gpg4win. The tester downloaded GPG and Kleopatra, and then tried running the gpg command again in Powershell. Once again, it failed, so they used the tutorial and tried running the provided commands. They successfully downloaded the checksum and the signature using wget, and then used Kleopatra to successfully verify the signature.
   
4. Flashing the usb key: 4min - 4 <br>
   The tester searched how to flash a USB key on google. The first link they selected was for rufus. They downloaded rufus and used it to successfully flash the USB key.

5. Booting from the usb key: 12min - 2 <br>
   The tester successfully interrupted the startup, and then used the verbose. When they couldn't find the boot menu, they tried booting into the pre-installed debian system to use the initial menu options in GRUB to change the boot order. When that didn't work, they referred to the hint, rebooted and interrupted startup, and then they entered the boot menu and successfully booted into the USB key.

### Part 2 - Proceed to the installation 

1. Installation type: 1min - 4 <br>
   The tester selected the *Graphical Install* option.

2. Language, location and keyboard: 1min - 4

3. Network configuration: <1min - 4

4. Hostname and domain name: 1min - 4 <br>
   The tester created a random hostname and domain name and proceeded.
   
5. Users configuration: <1min - 4
   
6. Clock configuration: 1min - 4 

7. Disk partitioning: 1min - 3 <br>
   The tester selected *Guided - use entire disk*, and nearly installed Debian onto the USB key before the observer intervened and told them what that would do. 

8. Network mirror: 3min - 4

9. Popularity Contest: 2min - 4 ("No")

10. Software Installation: 9min - 4 (installation took long)

### Part 3 - Reboot and explore your new system

1. Booting into the system: 1min - 4

2. Login: <1min - 4

3. Wallpaper change: 3min - 4

Tester Comment: The tester said that the concepts were unfamiliar in part 1 and that made it difficult for them to proceed. They suggested that more clear and simple documentation be provided for those unfamiliar with checksums, signatures, and flashing a USB.

## Tester 9 - September 1st, 2023 - Windows 11

### Time

From 10:26 AM to 12:39 PM

Took 2h13m total

### Part 1 - Prepare a bootable media with Debian 12

1. Debian 12 image download: 5min - 4
   The tester missed the big download button on debian.org but looked through documentation and found it.
   
2. Checksum verification: 23min - 3 <br>
   The tester found the checksum file and looked through the early pages of the installation guide in the Debian documentation. They nearly skipped the task, but were told there was more to be done. They researched the task on google and found videos and documentation from 3rd party sources. They attempted to use linux commands in the windows CMD. Then, the tester used the verbose and found documentation from the code signing website that explains the certutil commasnd and how to use it. Then, they successfully verified the checksum.

3. Signature verification: 44min - 1 <br>
   The tester looked through the documentation on the Debian download page to understand how to verify a signature. They attempted to copy the public keys into the windows CMD, and when that didn't work, they used the verbose. Afterwards, they wen tback to the signature verification docs and researched GnuPG. Then, they used the hint, searched GnuPG ddownload on google, and downloaded and installed gpg4win. The tester then tried to run some gpg commands on powershell but they didn't work. Next, they found a tutorial on Kleopatra and was able to generate new signing keys. Then, the tester used the tutorial, and needed help from the observer to understand how to download the checksum and the signature from the Debian website. Then they were able to use the tutorial to successfully verify the signature. 
   
4. Flashing the usb key: 7min - 4 <br>
   The tester initially tried to copy+paste the iso onto the usb key. When told that it wouldn't work, they researched the task on Google and found a tutorial on the lifewire website. Using that tutorial, they downloaded rufus and successfully flashed the iso onto the USB.
   
5. Booting from the usb key: 16min - 3 <br>
   The tester immediately used the verbose, and then found the startup interrupt. Next, they entered setup and changed the boot order and successfully booted into the USB key
   

### Part 2 - Proceed to the installation 

1. Installation type: <1min - 4 <br>
   The tester selected the *Install* option.

2. Language, location and keyboard: 1min - 4

3. Network configuration: 2min - 4

4. Hostname and domain name: 1min - 4 <br>

5. Users configuration: 3min - 4
   
6. Clock configuration: 1min - 4 

7. Disk partitioning: 9min - 3 <br>
   The tester nearly installed the system on the USB and needed some assistance to restart the partitioning process.

8. Network mirror: 1min - 4

9. Popularity Contest: 1min - 4 ("No")

10. Software Installation: 15min - 4 (installation took long)

### Part 3 - Reboot and explore your new system

1. Booting into the system: <1min - 4

2. Login: 1min - 4

3. Wallpaper change: 2min - 4

Tester Comment: The tester found there was a lot of documentation that didn't really say anything meaningful for beginners, rather they suggested more clear documentation for the entirety of part 1.