# Data Analysis & Report on Usability Tests

Taking into consideration the usability tests that were performed with the help of 10 participants and the notes compiled by the observers, this will give a brief report on our findings of each task relating to the installation process, including time taken and grading scheme.

## Time

The average time taken was 1h42min. This is including time waiting for packages to install, amongst others.

## General Information

All testers had access to the same information and explanations, whether verbal and provided by the observers or through the [documentation](/installation/README.md) prepared in advance. They all used the same devices and performed the tasks in the same environment.

## Part 1 - Preparing for the installation

In this section, we asked testers to follow instructions provided by Debian to install the image file, verify it by using the checksum and signature provided, flash the image unto a usb key and then booting into the usb key.

This section was the most difficult for all participants and in consequence, took the longest. The overall consensus was that there were complicated concepts involved that were hard to understand and even harder to figure out. Furthermore, although Debian had documentation about it, for example this [checksum documentation](https://www.debian.org/CD/verify), it was nearly impossible for people with no technical background and no previous experience with subjects of the sort to accomplish the task or understand what was being asked of them. The overall feedback was that the documentation was worded in a confusing way and was not helpful in actually completing the task, and that it needed more instructions, images or even links to tutorials. Furthermore, some of the things that seemed confusing to people regarding the documentation were the algorithm used for the checksum (SHA512) and the name of the links (SHA512SUMS and signature) and the reason why these steps were necessary. It would be a nice addition if we could include that it's important to verify the checkum and signature in order to assure that our users are not downloading a corrupt file or a virus.

Moreover, a lot of our testers did not understand what flashing a USB key meant or how to do it and most of them tried to copy paste the Debian 12 image onto the USB key. A quick way to deal with this would be to add suggestion of tools and/or software that could be used to flash a usb key, for example, **BalenaEtcher** or the **dd** command.

On another note, if we want to accomodate people of all backgrounds, it would also be helpful if we could provide tools, or easy ways to accomplish tasks 2, 3 and 4 (verifying the checksum, signature and flashing a usb key) as not everyone necessarily wants or needs to understand what a checksum and/or signature is, what it's used for, etc. The main point of this is to allow people to have the most seamless experience possible installing Debian, and to make sure that even those with no prior knowledge are able to.

Some feedback received from the testers was that, because part 1 included a lot of unfamiliar terms, we might benefit from adding documentation on how to verify a checksum and the signature directly on the download page, as well as maybe a list of software supporting flashing a usb key for different systems (Windows, Linux and Mac).

Some links that the testers needed in order to accomplish the tasks associated with this section:

- [What Is a Checksum?](https://www.howtogeek.com/363735/what-is-a-checksum-and-why-should-you-care/), by Chris Hoffman
- [How to Check a File Checksum](https://codesigningstore.com/how-to-check-file-checksum), by Code Signing Store
- [How to Verify MD5 Checksum of Files Using certutil](https://www.thewindowsclub.com/verify-md5-checksum-of-files-using-certutil), by The Windows Club
- [How to Create a Bootable Linux USB Drive](https://www.zdnet.com/article/how-to-create-a-bootable-linux-usb-drive/), by Jack Wallen
- [How to Create a Bootable Flash Drive](https://linuxhint.com/create_bootable_linux_usb_flash_drive/), by Linux Hint


## Part 2 - Installation

In this section, testers were asked to go through the process of installing Debian. This is delimitated by booting into the USB key containing the Debian 12 image, starting at choosing the installation type (Graphical Installation, amongst others).

This section went fairly well, the process of installation was, for the most part, simple and straight-forward. However, most of our testers had problems with some parts of it, namely choosing a hostname and domain name, disk partitioning, and network mirror. They also seemed unsure of which installation type to choose.

According to our observations, users with no technical experience would benefit from the addition of one of two things: a default option for the different parts of the installation (like for example, a hostname and domain name, network mirror) or making the installation simpler, including fewer questions and then adding an "Advanced Options" button or menu option where more specific system configurations would reside, such as manual partitioning, hostname and domain name as well as the network mirror meant for more experienced users who might need more specific configuration options. On another note, we might want to specify the clock configuration (timezone) along with the language, location and keyboard to better section the installation.

Overall, the installation process itself went fairly smoothly and most users were able to get through it without much intervention or the need of explanations or extra documentation.


## Part 3 - Exploring the new system

Note: all of our testers decided to install Gnome as their desktop environment so the comments below refer to the usability of Gnome and not Debian itself

In this section, we asked testers to boot into their new Debian sytem, login to their user accounts and perform some simple tasks.

This section went very well, and the tasks were performed rather intuitively and not much research was needed. We asked testers to change their wallpapers and open a few windows and send the observer an email with any feedback they might want to give us. They were able to perform all of the above with little struggle. However, one suggestion that was transmitted to us was to lock the taskbar as default and make the applications tab better visible to users (on Gnome, it appears as text in the upper left corner of the screen).

