# Informed Consent Form

**Project title** 

Testing the usability of the Debian installation process.


**Study investigators/observers**

Principal Investigator: Giuliana Bouzon (<ga@bouzon.com.br>);
Co-investigators: Tássia Camões Araújo (<tassia@debian.org>) and Anthony Nadeau (<tonynadeau03@gmail.com>).


**Funding source**

This study is run on a volunteer basis, as all Debian contributions. One of the computers used in the study is funded by donations made to the Debian project.


**Invitation to participate** 

You are being invited to participate in a research study. Choosing whether or not to participate is entirely your choice. If you decide not to participate, there will be no negative impacts on your relationship with the researcher(s). The information provided in this form tells you about what is involved in the research, what you will be asked to do, and any potential risks or benefits. Please read this form carefully, take all the time you need, and ask any questions you may have. 

Consent is an ongoing process. During the research study, we will tell you about any significant finding that could affect your willingness to continue to participate in this study.


**Purpose of the research study**

Debian is an open source Unix-like operating system that is constantly being developed by a community of volunteers around the globe. The goal of this research is to assess how usable the installation process of the operating system is for users with varying experience performing such a task. This information will lead to clear guidelines that contributors of Debian can follow to improve the installer and facilitate the process for people choosing to use the system.


**What you will be asked to do**

If you decide to participate in this research, you will be asked to attend an appointment of roughly two hours with one of our observers at Vanier College campus. During this session, you will be provided with 2 laptops for the duration of the appointment, and a list of tasks related to the Debian installation process. You will then be asked to perform those tasks, installing Debian on one of the laptops, while using the second laptop to perform auxiliary tasks and web researches as needed.


**Who can take part in the research study?**

Anyone can participate in this study. There are no restrictions on the category of people who may partake.


**Possible risks and benefits**

**Risks:** 

As the initial instructions provided are limited to what is available on the
Debian website (subject of the present test), there is the
possibility that some tasks may be hard to understand, and this can raise
feelings of discomfort. If this happens, a browser is available to research,
and more information will be provided if requested. The observer will be
available at all times, and will make sure you can proceed to the next step. The fact that someone is observing your actions might also be intimidating at first, but as the session proceeds and you get concentrated in the process, this feeling tends to diminish.


**Benefits:** 

There is no guarantee that you will benefit directly from
participating in this study. However, the study can provide you with knowledge
of how to install the Debian operating system if you do not already know. In
addition to that, recognition for your participation will be given in any
produced report, if you wish to have your name listed. Finally, as a
symbolic compensation, you'll be offered a chocolate bar at the end of the
observation session.


**Privacy and confidentiality**

All data collected by the observer will be void of personal information. This means that no data will be associated with you specifically. The information we collect about your tests will solely be identified by a numeric identifier, for example, “tester 1”.


**Withdrawing from the study**

Your participation in this study is completely voluntary, and you are under no obligation to participate. If you decide to participate but change your mind later on, you are free to withdraw at any time without consequence. Your decision to withdraw will not influence your relationship with the researcher in any way. If you decide to withdraw after the appointment, any data collected from your tests will be discarded.


**Questions and contact information**

If you have any questions about the study or would like more information please feel free to contact

    Giuliana Bouzon 
    ga@bouzon.com.br

___

## Signature Page

**Project title:** Testing the usability of the Debian installation process.

**Lead researcher:** Giuliana Bouzon


**Statement of consent**

By signing this form, I agree that:

- The study has been explained to me
- All my questions have been answered
- Possible harm and discomforts and possible benefits (if any) of this study have been explained to me
- I have been told that my personal information will be kept confidential

In addition, I understand that:
- I have the right not to participate and the right to stop at any time
- I may refuse to participate without consequence
- I am free now, and in the future, to ask any questions about the study
- No information that would identify me will be released or printed without asking me first
- I will receive a signed copy of this consent form.



Name:__________________________________________________________________	 Date:______________________________________


Signature:____________________________________________________________

**Signature of the person obtaining consent**

By signing this form, I attest that:

- I have explained the study to the prospective participant
- I answered all of their questions
- I provided a copy of this consent form to the participant
- The participant seemed to understand the consent form and agreed to participate



Name:__________________________________________________________________  Date:______________________________________


Signature:____________________________________________________________
