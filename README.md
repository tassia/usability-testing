# Usability testing in Debian

We use this repo to collaborate on the planning, execution and result
consolidation of usability tests in Debian.

## Testing rounds

We are planning on having multiple rounds of usability tests in Debian, to cover different aspects of the project. Each phase has a dedicated folder, as listed below:

* [Phase 1: Installer (2023)](https://salsa.debian.org/tassia/usability-testing/-/tree/main/Installer%20-%202023)
* [Phase 2: Installer (2024)](https://salsa.debian.org/tassia/usability-testing/-/tree/main/Installer%20-%202024)

## Contributing

Comments, suggestions and merge requests are more than welcome! You can reach us
through email or through
[Discord](https://discord.gg/rBKb9JRQGD).

We are also open to organizing usability tests in other areas of Debian or FLOSS
in general.

## Previous initiatives and references

* [GNOME and Debian usability testing, May 2017](https://people.debian.org/~intrigeri/blog/posts/GNOME_and_Debian_usability_testing_201705/)
* [GNOME Usability Test Results, Part 1](https://renatagegaj.wordpress.com/2016/08/23/gnome-usability-test-results-part-1/) and [Part 2](https://renatagegaj.wordpress.com/2016/08/26/gnome-usability-test-results-part-2/), by Renata Gegaj
* [It's about the User: Applying Usability in Open-Source Software
](https://www.linuxjournal.com/content/its-about-user-applying-usability-open-source-software), by Jim Hall
* [State Of Linux Usability 2020](https://lawzava.com/blog/state-of-linux-usability-2020/), Law Zava
* [Usability testing: how do we design effective tasks](https://ubuntu.com/blog/usability-testing-how-do-we-design-effective-tasks),
by Canonical
* [Usability Testing & Evaluation of Chores in GNU/Linux for Novice](http://www.diva-portal.org/smash/get/diva2:832464/FULLTEXT01.pdf), by Muhammad Hamid Fayyaz & Usman Idrees
* [Usability Testing Artifacts](https://github.com/clarissalimab/ux), by Clarissa Lima
* [Usability](https://wiki.debian.org/Usability), article on Debian Wiki
